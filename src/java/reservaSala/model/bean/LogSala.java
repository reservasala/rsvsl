/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@Entity
@Table(name = "log_sala")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogSala.findAll", query = "SELECT l FROM LogSala l"),
    @NamedQuery(name = "LogSala.findByDataHoraChaveRet", query = "SELECT l FROM LogSala l WHERE l.dataHoraChaveRet = :dataHoraChaveRet"),
    @NamedQuery(name = "LogSala.findByDataHoraChaveDevo", query = "SELECT l FROM LogSala l WHERE l.dataHoraChaveDevo = :dataHoraChaveDevo"),
    @NamedQuery(name = "LogSala.findByLogSalaId", query = "SELECT l FROM LogSala l WHERE l.logSalaId = :logSalaId")})
public class LogSala implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "data_hora_chave_ret")
    @Temporal(TemporalType.TIME)
    private Date dataHoraChaveRet;
    @Column(name = "data_hora_chave_devo")
    @Temporal(TemporalType.TIME)
    private Date dataHoraChaveDevo;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "log_sala_id")
    private Integer logSalaId;
    @JoinColumn(name = "assistente", referencedColumnName = "assistente_id")
    @ManyToOne(optional = false)
    private Assistente assistente;
    @JoinColumn(name = "professor", referencedColumnName = "professor_id")
    @ManyToOne(optional = false)
    private Professor professor;
    @JoinColumn(name = "sala", referencedColumnName = "sala_id")
    @ManyToOne(optional = false)
    private Sala sala;

    public LogSala() {
    }

    public LogSala(Integer logSalaId) {
        this.logSalaId = logSalaId;
    }

    public LogSala(Integer logSalaId, Date dataHoraChaveRet) {
        this.logSalaId = logSalaId;
        this.dataHoraChaveRet = dataHoraChaveRet;
    }

    public Date getDataHoraChaveRet() {
        return dataHoraChaveRet;
    }

    public void setDataHoraChaveRet(Date dataHoraChaveRet) {
        this.dataHoraChaveRet = dataHoraChaveRet;
    }

    public Date getDataHoraChaveDevo() {
        return dataHoraChaveDevo;
    }

    public void setDataHoraChaveDevo(Date dataHoraChaveDevo) {
        this.dataHoraChaveDevo = dataHoraChaveDevo;
    }

    public Integer getLogSalaId() {
        return logSalaId;
    }

    public void setLogSalaId(Integer logSalaId) {
        this.logSalaId = logSalaId;
    }

    public Assistente getAssistente() {
        return assistente;
    }

    public void setAssistente(Assistente assistente) {
        this.assistente = assistente;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logSalaId != null ? logSalaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogSala)) {
            return false;
        }
        LogSala other = (LogSala) object;
        if ((this.logSalaId == null && other.logSalaId != null) || (this.logSalaId != null && !this.logSalaId.equals(other.logSalaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reservaSala.model.bean.LogSala[ logSalaId=" + logSalaId + " ]";
    }
    
}
