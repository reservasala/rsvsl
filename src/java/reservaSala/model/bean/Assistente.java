/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jorge
 */
@Entity
@Table(name = "assistente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Assistente.findAll", query = "SELECT a FROM Assistente a"),
    @NamedQuery(name = "Assistente.findByAssistenteId", query = "SELECT a FROM Assistente a WHERE a.assistenteId = :assistenteId"),
    @NamedQuery(name = "Assistente.findByNome", query = "SELECT a FROM Assistente a WHERE a.nome = :nome"),
    @NamedQuery(name = "Assistente.findByEndereco", query = "SELECT a FROM Assistente a WHERE a.endereco = :endereco"),
    @NamedQuery(name = "Assistente.findByCpf", query = "SELECT a FROM Assistente a WHERE a.cpf = :cpf")})
public class Assistente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "assistente_id")
    private Integer assistenteId;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Column(name = "endereco")
    private String endereco;
    @Basic(optional = false)
    @Column(name = "cpf")
    private String cpf;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assistente")
    private List<LogEquipamento> logEquipamentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assistente")
    private List<LogSala> logSalaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assistente")
    private List<DefeitoEquipamento> defeitoEquipamentoList;

    public Assistente() {
    }

    public Assistente(Integer assistenteId) {
        this.assistenteId = assistenteId;
    }

    public Assistente(Integer assistenteId, String nome, String cpf) {
        this.assistenteId = assistenteId;
        this.nome = nome;
        this.cpf = cpf;
    }

    public Integer getAssistenteId() {
        return assistenteId;
    }

    public void setAssistenteId(Integer assistenteId) {
        this.assistenteId = assistenteId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @XmlTransient
    public List<LogEquipamento> getLogEquipamentoList() {
        return logEquipamentoList;
    }

    public void setLogEquipamentoList(List<LogEquipamento> logEquipamentoList) {
        this.logEquipamentoList = logEquipamentoList;
    }

    @XmlTransient
    public List<LogSala> getLogSalaList() {
        return logSalaList;
    }

    public void setLogSalaList(List<LogSala> logSalaList) {
        this.logSalaList = logSalaList;
    }

    @XmlTransient
    public List<DefeitoEquipamento> getDefeitoEquipamentoList() {
        return defeitoEquipamentoList;
    }

    public void setDefeitoEquipamentoList(List<DefeitoEquipamento> defeitoEquipamentoList) {
        this.defeitoEquipamentoList = defeitoEquipamentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assistenteId != null ? assistenteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assistente)) {
            return false;
        }
        Assistente other = (Assistente) object;
        if ((this.assistenteId == null && other.assistenteId != null) || (this.assistenteId != null && !this.assistenteId.equals(other.assistenteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reservaSala.model.bean.Assistente[ assistenteId=" + assistenteId + " ]";
    }
    
}
