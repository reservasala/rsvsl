/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@Entity
@Table(name = "defeito_equipamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DefeitoEquipamento.findAll", query = "SELECT d FROM DefeitoEquipamento d"),
    @NamedQuery(name = "DefeitoEquipamento.findByDefeitoId", query = "SELECT d FROM DefeitoEquipamento d WHERE d.defeitoId = :defeitoId"),
    @NamedQuery(name = "DefeitoEquipamento.findByDescricao", query = "SELECT d FROM DefeitoEquipamento d WHERE d.descricao = :descricao"),
    @NamedQuery(name = "DefeitoEquipamento.findByDataDefeito", query = "SELECT d FROM DefeitoEquipamento d WHERE d.dataDefeito = :dataDefeito")})
public class DefeitoEquipamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "defeito_id")
    private Integer defeitoId;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "data_defeito")
    @Temporal(TemporalType.DATE)
    private Date dataDefeito;
    @JoinColumn(name = "assistente", referencedColumnName = "assistente_id")
    @ManyToOne(optional = false)
    private Assistente assistente;
    @JoinColumn(name = "equipamento", referencedColumnName = "equipamento_id")
    @ManyToOne(optional = false)
    private Equipamento equipamento;

    public DefeitoEquipamento() {
    }

    public DefeitoEquipamento(Integer defeitoId) {
        this.defeitoId = defeitoId;
    }

    public DefeitoEquipamento(Integer defeitoId, String descricao, Date dataDefeito) {
        this.defeitoId = defeitoId;
        this.descricao = descricao;
        this.dataDefeito = dataDefeito;
    }

    public Integer getDefeitoId() {
        return defeitoId;
    }

    public void setDefeitoId(Integer defeitoId) {
        this.defeitoId = defeitoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataDefeito() {
        return dataDefeito;
    }

    public void setDataDefeito(Date dataDefeito) {
        this.dataDefeito = dataDefeito;
    }

    public Assistente getAssistente() {
        return assistente;
    }

    public void setAssistente(Assistente assistente) {
        this.assistente = assistente;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (defeitoId != null ? defeitoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DefeitoEquipamento)) {
            return false;
        }
        DefeitoEquipamento other = (DefeitoEquipamento) object;
        if ((this.defeitoId == null && other.defeitoId != null) || (this.defeitoId != null && !this.defeitoId.equals(other.defeitoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reservaSala.model.bean.DefeitoEquipamento[ defeitoId=" + defeitoId + " ]";
    }
    
}
