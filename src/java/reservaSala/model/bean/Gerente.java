/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jorge
 */
@Entity
@Table(name = "gerente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gerente.findAll", query = "SELECT g FROM Gerente g"),
    @NamedQuery(name = "Gerente.findByGerenteId", query = "SELECT g FROM Gerente g WHERE g.gerenteId = :gerenteId"),
    @NamedQuery(name = "Gerente.findByNome", query = "SELECT g FROM Gerente g WHERE g.nome = :nome"),
    @NamedQuery(name = "Gerente.findByEndereco", query = "SELECT g FROM Gerente g WHERE g.endereco = :endereco"),
    @NamedQuery(name = "Gerente.findByCpf", query = "SELECT g FROM Gerente g WHERE g.cpf = :cpf")})
public class Gerente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gerente_id")
    private Integer gerenteId;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Column(name = "endereco")
    private String endereco;
    @Column(name = "cpf")
    private String cpf;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gerenteCadastro")
    private List<Professor> professorList;

    public Gerente() {
    }

    public Gerente(Integer gerenteId) {
        this.gerenteId = gerenteId;
    }

    public Gerente(Integer gerenteId, String nome) {
        this.gerenteId = gerenteId;
        this.nome = nome;
    }

    public Integer getGerenteId() {
        return gerenteId;
    }

    public void setGerenteId(Integer gerenteId) {
        this.gerenteId = gerenteId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @XmlTransient
    public List<Professor> getProfessorList() {
        return professorList;
    }

    public void setProfessorList(List<Professor> professorList) {
        this.professorList = professorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gerenteId != null ? gerenteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gerente)) {
            return false;
        }
        Gerente other = (Gerente) object;
        if ((this.gerenteId == null && other.gerenteId != null) || (this.gerenteId != null && !this.gerenteId.equals(other.gerenteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reservaSala.model.bean.Gerente[ gerenteId=" + gerenteId + " ]";
    }
    
}
