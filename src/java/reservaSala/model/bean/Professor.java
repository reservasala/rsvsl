/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jorge
 */
@Entity
@Table(name = "professor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Professor.findAll", query = "SELECT p FROM Professor p"),
    @NamedQuery(name = "Professor.findByProfessorId", query = "SELECT p FROM Professor p WHERE p.professorId = :professorId"),
    @NamedQuery(name = "Professor.findByNome", query = "SELECT p FROM Professor p WHERE p.nome = :nome"),
    @NamedQuery(name = "Professor.findByEndereco", query = "SELECT p FROM Professor p WHERE p.endereco = :endereco"),
    @NamedQuery(name = "Professor.findByCpf", query = "SELECT p FROM Professor p WHERE p.cpf = :cpf")})
public class Professor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "professor_id")
    private Integer professorId;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Column(name = "endereco")
    private String endereco;
    @Basic(optional = false)
    @Column(name = "cpf")
    private String cpf;
    @JoinColumn(name = "gerente_cadastro", referencedColumnName = "gerente_id")
    @ManyToOne(optional = false)
    private Gerente gerenteCadastro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "professor")
    private List<LogEquipamento> logEquipamentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "professor")
    private List<LogSala> logSalaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "professor")
    private List<Agenda> agendaList;

    public Professor() {
    }

    public Professor(Integer professorId) {
        this.professorId = professorId;
    }

    public Professor(Integer professorId, String nome, String cpf) {
        this.professorId = professorId;
        this.nome = nome;
        this.cpf = cpf;
    }

    public Integer getProfessorId() {
        return professorId;
    }

    public void setProfessorId(Integer professorId) {
        this.professorId = professorId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Gerente getGerenteCadastro() {
        return gerenteCadastro;
    }

    public void setGerenteCadastro(Gerente gerenteCadastro) {
        this.gerenteCadastro = gerenteCadastro;
    }

    @XmlTransient
    public List<LogEquipamento> getLogEquipamentoList() {
        return logEquipamentoList;
    }

    public void setLogEquipamentoList(List<LogEquipamento> logEquipamentoList) {
        this.logEquipamentoList = logEquipamentoList;
    }

    @XmlTransient
    public List<LogSala> getLogSalaList() {
        return logSalaList;
    }

    public void setLogSalaList(List<LogSala> logSalaList) {
        this.logSalaList = logSalaList;
    }

    @XmlTransient
    public List<Agenda> getAgendaList() {
        return agendaList;
    }

    public void setAgendaList(List<Agenda> agendaList) {
        this.agendaList = agendaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (professorId != null ? professorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Professor)) {
            return false;
        }
        Professor other = (Professor) object;
        if ((this.professorId == null && other.professorId != null) || (this.professorId != null && !this.professorId.equals(other.professorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reservaSala.model.bean.Professor[ professorId=" + professorId + " ]";
    }
    
}
