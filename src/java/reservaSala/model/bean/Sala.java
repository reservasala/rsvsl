/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jorge
 */
@Entity
@Table(name = "sala")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sala.findAll", query = "SELECT s FROM Sala s"),
    @NamedQuery(name = "Sala.findByPredio", query = "SELECT s FROM Sala s WHERE s.predio = :predio"),
    @NamedQuery(name = "Sala.findByCampus", query = "SELECT s FROM Sala s WHERE s.campus = :campus"),
    @NamedQuery(name = "Sala.findByNumero", query = "SELECT s FROM Sala s WHERE s.numero = :numero"),
    @NamedQuery(name = "Sala.findBySalaId", query = "SELECT s FROM Sala s WHERE s.salaId = :salaId")})
public class Sala implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "predio")
    private String predio;
    @Column(name = "campus")
    private String campus;
    @Basic(optional = false)
    @Column(name = "numero")
    private int numero;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sala_id")
    private Integer salaId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sala")
    private List<LogSala> logSalaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sala")
    private List<Agenda> agendaList;

    public Sala() {
    }

    public Sala(Integer salaId) {
        this.salaId = salaId;
    }

    public Sala(Integer salaId, String predio, int numero) {
        this.salaId = salaId;
        this.predio = predio;
        this.numero = numero;
    }

    public String getPredio() {
        return predio;
    }

    public void setPredio(String predio) {
        this.predio = predio;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Integer getSalaId() {
        return salaId;
    }

    public void setSalaId(Integer salaId) {
        this.salaId = salaId;
    }

    @XmlTransient
    public List<LogSala> getLogSalaList() {
        return logSalaList;
    }

    public void setLogSalaList(List<LogSala> logSalaList) {
        this.logSalaList = logSalaList;
    }

    @XmlTransient
    public List<Agenda> getAgendaList() {
        return agendaList;
    }

    public void setAgendaList(List<Agenda> agendaList) {
        this.agendaList = agendaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salaId != null ? salaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sala)) {
            return false;
        }
        Sala other = (Sala) object;
        if ((this.salaId == null && other.salaId != null) || (this.salaId != null && !this.salaId.equals(other.salaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reservaSala.model.bean.Sala[ salaId=" + salaId + " ]";
    }
    
}
