/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.bean;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jorge
 */
@Entity
@Table(name = "log_equipamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogEquipamento.findAll", query = "SELECT l FROM LogEquipamento l"),
    @NamedQuery(name = "LogEquipamento.findByLogEquipamentoId", query = "SELECT l FROM LogEquipamento l WHERE l.logEquipamentoId = :logEquipamentoId"),
    @NamedQuery(name = "LogEquipamento.findByDataHoraRet", query = "SELECT l FROM LogEquipamento l WHERE l.dataHoraRet = :dataHoraRet"),
    @NamedQuery(name = "LogEquipamento.findByDataHoraDevo", query = "SELECT l FROM LogEquipamento l WHERE l.dataHoraDevo = :dataHoraDevo")})
public class LogEquipamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "log_equipamento_id")
    private Integer logEquipamentoId;
    @Basic(optional = false)
    @Column(name = "data_hora_ret")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHoraRet;
    @Column(name = "data_hora_devo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHoraDevo;
    @JoinColumn(name = "assistente", referencedColumnName = "assistente_id")
    @ManyToOne(optional = false)
    private Assistente assistente;
    @JoinColumn(name = "equipamento", referencedColumnName = "equipamento_id")
    @ManyToOne(optional = false)
    private Equipamento equipamento;
    @JoinColumn(name = "professor", referencedColumnName = "professor_id")
    @ManyToOne(optional = false)
    private Professor professor;

    public LogEquipamento() {
    }

    public LogEquipamento(Integer logEquipamentoId) {
        this.logEquipamentoId = logEquipamentoId;
    }

    public LogEquipamento(Integer logEquipamentoId, Date dataHoraRet) {
        this.logEquipamentoId = logEquipamentoId;
        this.dataHoraRet = dataHoraRet;
    }

    public Integer getLogEquipamentoId() {
        return logEquipamentoId;
    }

    public void setLogEquipamentoId(Integer logEquipamentoId) {
        this.logEquipamentoId = logEquipamentoId;
    }

    public Date getDataHoraRet() {
        return dataHoraRet;
    }

    public void setDataHoraRet(Date dataHoraRet) {
        this.dataHoraRet = dataHoraRet;
    }

    public Date getDataHoraDevo() {
        return dataHoraDevo;
    }

    public void setDataHoraDevo(Date dataHoraDevo) {
        this.dataHoraDevo = dataHoraDevo;
    }

    public Assistente getAssistente() {
        return assistente;
    }

    public void setAssistente(Assistente assistente) {
        this.assistente = assistente;
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logEquipamentoId != null ? logEquipamentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogEquipamento)) {
            return false;
        }
        LogEquipamento other = (LogEquipamento) object;
        if ((this.logEquipamentoId == null && other.logEquipamentoId != null) || (this.logEquipamentoId != null && !this.logEquipamentoId.equals(other.logEquipamentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "reservaSala.model.bean.LogEquipamento[ logEquipamentoId=" + logEquipamentoId + " ]";
    }
    
}
