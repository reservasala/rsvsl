/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.command;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reservaSala.dao.AgendaJpaController;
import reservaSala.model.bean.Agenda;
import reservaSala.model.connection.ConnectionFactory;
import reservaSala.util.Constants;

/**
 *
 * @author Jorge
 */
public class EditAgenda implements InterfaceCommand {

    EntityManagerFactory factory;
    private AgendaJpaController agendaDao;
    private Agenda agenda;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        
        if (request.getSession().getAttribute("isGerente").equals("true")) { 
            if (!request.getParameter("id").equals("") && request.getParameter("id") != null) {
                factory = ConnectionFactory.getEntityManagerFactory();
                agendaDao = new AgendaJpaController(factory);
                agenda = agendaDao.findAgenda(Integer.parseInt(request.getParameter("id")));
                if (("autorizar".equals(request.getParameter("acao")))) {
                    agenda.setStatus(Constants.ATENDIDO);
                } else if (("recusar".equals(request.getParameter("acao")))) {
                    agenda.setStatus(Constants.REJEITADO);
                }

                try {
                    agendaDao.edit(agenda);
                } catch (Exception ex) {
                    Logger.getLogger(EditAgenda.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return "ServletController?cmd=reservaSala.model.command.LoadSalaReservada";
        }
        return "home.jsp";
    }
}
