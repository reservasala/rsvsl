/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.command;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reservaSala.dao.AgendaJpaController;
import reservaSala.dao.LogSalaJpaController;
import reservaSala.dao.SalaJpaController;
import reservaSala.model.bean.Agenda;
import reservaSala.model.bean.LogSala;
import reservaSala.model.bean.Professor;
import reservaSala.model.connection.ConnectionFactory;
import reservaSala.util.Constants;

/**
 *
 * @author alexandre
 */
public class ReservaDeSala implements InterfaceCommand {

    private Date dataHoraChaveRet;
    private Date dataHoraChaveDevo;
    private Integer logSalaId;
    EntityManagerFactory factory;
    EntityManagerFactory factory1;

    private LogSalaJpaController logSalaDao;
    private LogSala logSala;
    private Agenda agenda;
    
    private AgendaJpaController agendaDao;
    private SalaJpaController salaDao;
    

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
            
        
        if(((request.getParameter("idSala").equals(""))&&(request.getParameter("dataHoraChaveRet").equals(""))&&(request.getParameter("dataHoraChaveDevo").equals("")))||((request.getParameter("idSala").equals(""))&&(request.getParameter("dataHoraChaveRet").equals("")))||((request.getParameter("idSala").equals(""))&&(request.getParameter("dataHoraChaveDevo").equals("")))||((request.getParameter("dataHoraChaveDevo").equals(""))&&(request.getParameter("dataHoraChaveRet").equals("")))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, preencha os dados obrigatorios!");
            request.setAttribute("mensage", "Por favor, preencha os dados obrigatorios!");
            return "reservaDeSala.jsp";           
        }else if (request.getParameter("idSala").equals("")) {
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, não se esqueça de inserir a sala!");
            request.setAttribute("mensage", "Por favor, não se esqueça de inserir a sala!");
            return "reservaDeSala.jsp";
        } else if((request.getParameter("dataHoraChaveRet").equals(""))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, não se esqueça de inserir a data de inicio!");
            request.setAttribute("mensage", "Por favor, não se esqueça de inserir a data de inicio!");
            return "reservaDeSala.jsp";
        } else if((request.getParameter("dataHoraChaveDevo").equals(""))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, não se esqueça de inserir a data de fim!");
            request.setAttribute("mensage", "Por favor, não se esqueça de inserir a data de fim!");
            return "reservaDeSala.jsp";
        }
        
        try{
            int numero = Integer.parseInt(request.getParameter("idSala"));
            
        }catch(NumberFormatException ex){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira uma sala valida!");
            request.setAttribute("mensage", "Por favor, insira uma sala valida!");
            return "reservaDeSala.jsp";
        }
        
        try{
            String teste1 = request.getParameter("dataHoraChaveRet");
            if((teste1.charAt(2)!='/')||(teste1.charAt(5)!='/')||(teste1.charAt(10)!=' ')||(teste1.charAt(13)!=':')){
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Por favor, insira uma data de inicio valida!");
                request.setAttribute("mensage", "Por favor, insira uma data de inicio valida!");
                return "reservaDeSala.jsp";
            }
            
            String teste2 = request.getParameter("dataHoraChaveDevo");
            if((teste2.charAt(2)!='/')||(teste2.charAt(5)!='/')||(teste2.charAt(10)!=' ')||(teste2.charAt(13)!=':')){
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Por favor, insira uma data de termino valida!");
                request.setAttribute("mensage", "Por favor, insira uma data de termino valida!");
                return "reservaDeSala.jsp";
            }
            
            int dia1 = Integer.parseInt(teste1.substring(0, 2));
            int dia2 = Integer.parseInt(teste2.substring(0, 2));
            
            System.out.println(dia2);
            
            int mes1 = Integer.parseInt(teste1.substring(3, 5));
            int mes2 = Integer.parseInt(teste2.substring(3, 5));
            
            int ano1 = Integer.parseInt(teste1.substring(6, 10));
            int ano2 = Integer.parseInt(teste2.substring(6, 10)); 
            
            int horas1 = Integer.parseInt(teste1.substring(11, 13));
            int horas2 = Integer.parseInt(teste2.substring(11, 13));
            
            int minutos1 = Integer.parseInt(teste1.substring(14, 16));
            int minutos2 = Integer.parseInt(teste2.substring(14, 16));
            
            
            if((ano1<2016)||((ano1==2016)&&mes1<3)||((ano1==2016)&&(mes1==3)&&(dia1<31))){
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Por favor, insira uma data de inicio valida!");
                request.setAttribute("mensage", "Por favor, insira uma data de inicio valida!");
                return "reservaDeSala.jsp";
            }
            
            if((ano1>ano2)||(ano1==ano2 && mes1>mes2)||(ano1==ano2 && mes1==mes2 && dia1>dia2)){
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Por favor, insira uma data de termino valida!");
                request.setAttribute("mensage", "Por favor, insira uma data de termino valida!");
                return "reservaDeSala.jsp";
            }else if((ano1==ano2 && mes1==mes2 && dia1==dia2)&&((horas1>horas2)||(horas1==horas2 && minutos1>=minutos2))){
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Por favor, insira datas validas!");
                request.setAttribute("mensage", "Por favor, insira datas validas!");
                return "reservaDeSala.jsp";
            }
            
            if(dia1>31||mes1>12||horas1>23||minutos1>59){
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Por favor, insira uma data de inicio valida!");
                request.setAttribute("mensage", "Por favor, insira uma data de inicio valida!");
                return "reservaDeSala.jsp";
            }
            if(dia2>31||mes2>12||horas2>23||minutos2>59){
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Por favor, insira uma data de termino valida!");
                request.setAttribute("mensage", "Por favor, insira uma data de termino valida!");
                return "reservaDeSala.jsp";
            }
                       
            
        }catch(NumberFormatException ex){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira uma data valida!");
            request.setAttribute("mensage", "Por favor, insira uma data valida!");
            return "reservaDeSala.jsp";
        }
        
        
        
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        agenda = new Agenda();
        factory = ConnectionFactory.getEntityManagerFactory();
        
        logSalaDao = new LogSalaJpaController(factory);
        agendaDao= new AgendaJpaController(factory);
        salaDao = new SalaJpaController(factory);

        
        logSalaId = Integer.parseInt(request.getParameter("idSala"));
     
        try {
            dataHoraChaveRet = (Date) formatter.parse(request.getParameter("dataHoraChaveRet"));
            dataHoraChaveDevo = (Date) formatter.parse(request.getParameter("dataHoraChaveDevo"));

        } catch (ParseException ex) {
            Logger.getLogger(ReservaDeSala.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        agenda.setDataHoraInicio(dataHoraChaveRet);
        agenda.setDataHoraFim(dataHoraChaveDevo);
        agenda.setProfessor((Professor) request.getSession().getAttribute("user"));
        agenda.setSala(salaDao.findSalaByNumero(logSalaId));
        agenda.setStatus(Constants.SOLICITADO);
        
        
        
        agendaDao.create(agenda);
        
        
        request.setAttribute("msg", true);
        request.setAttribute("msg_txt", "Pedido de Reserva concluído!");
        request.setAttribute("mensage", "Pedido de Reserva concluído!");
        
        return "home.jsp";
    }
}
