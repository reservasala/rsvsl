/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.command;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reservaSala.dao.AgendaJpaController;
import reservaSala.model.bean.Agenda;
import reservaSala.model.connection.ConnectionFactory;

/**
 *
 * @author Jorge
 */
public class LoadSalaAgendada implements InterfaceCommand {

    private Agenda agenda;

    EntityManagerFactory factory;
    private AgendaJpaController agendaDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        if (request.getParameter("id") == null || "".equals(request.getParameter("id"))) {
            return "home.jsp";
        }
        factory = ConnectionFactory.getEntityManagerFactory();
        agendaDao = new AgendaJpaController(factory);
        agenda = agendaDao.findAgenda(Integer.parseInt(request.getParameter("id")));
        
        request.setAttribute("agenda", agenda);
        request.setAttribute("status", agenda.getStatus()+"");
        request.setAttribute("agendaId", agenda.getAgendaId());
        
        return "carregaSala.jsp";
    }

}
