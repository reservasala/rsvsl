/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.command;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reservaSala.model.connection.ConnectionFactory;
import reservaSala.dao.ProfessorJpaController;
import reservaSala.dao.GerenteJpaController;
import reservaSala.dao.AssistenteJpaController;
import reservaSala.model.bean.Professor;
import reservaSala.model.bean.Gerente;
import reservaSala.model.bean.Assistente;

public class UserLogin implements InterfaceCommand {

    private ProfessorJpaController professorDao;
    private GerenteJpaController gerenteDao;
    private AssistenteJpaController assistenteDao;

    public UserLogin() {

    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Professor professor;
        Gerente gerente;
        Assistente assistente;
        String login = request.getParameter("login");
        if ((login == null) || (request.getParameter("password") == null)) {
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Usu&aacute;rio e senha n&atilde;o podem ser nulos!");
            return "login.jsp";
        }

        EntityManagerFactory factory = ConnectionFactory.getEntityManagerFactory();
        professorDao = new ProfessorJpaController(factory);
        gerenteDao = new GerenteJpaController(factory);
        professor = professorDao.findProfessorByLogin(login);
        gerente = gerenteDao.findGerenteByLogin(login);

        if (professor == null || !professor.getCpf().equals(request.getParameter("password"))) {
            if (gerente == null || !gerente.getCpf().equals(request.getParameter("password"))) {
                request.setAttribute("msg", true);
                request.setAttribute("msg_txt", "Login ou senha inv&aacute;lido!");
                request.setAttribute("mensage", "Login ou senha inválido");
                return "login.jsp";
            }
        }

        String isProfessor;
        String isGerente;

        if (professor != null) {
            request.getSession().setAttribute("user", professor);
            isProfessor = "true";
            isGerente = "false";
            request.getSession().setAttribute("id", professor.getProfessorId());
            request.getSession().setAttribute("username", professor.getNome());
        } else {
            request.getSession().setAttribute("user", gerente);
            isProfessor = "false";
            isGerente = "true";
            request.getSession().setAttribute("id", gerente.getGerenteId());
            request.getSession().setAttribute("username", gerente.getNome());
        }
        request.getSession().setAttribute("isProfessor", isProfessor);
        request.getSession().setAttribute("isGerente", isGerente);

        return "home.jsp";
    }

}
