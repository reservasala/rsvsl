/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.command;

import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reservaSala.dao.AgendaJpaController;
import reservaSala.model.bean.Agenda;
import reservaSala.model.bean.Professor;
import reservaSala.model.connection.ConnectionFactory;
import reservaSala.util.Constants;

/**
 *
 * @author Jorge
 */
public class LoadSalaReservada implements InterfaceCommand {

    private List<Agenda> listaSolicitados;
    private List<Agenda> listaAtendidos;
    private List<Agenda> listaRejeitados;
    private List<Agenda> listaFinalizados;
    EntityManagerFactory factory;
    
    private AgendaJpaController agendaDao;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        factory = ConnectionFactory.getEntityManagerFactory();
        agendaDao = new AgendaJpaController(factory);
        if (request.getSession().getAttribute("isGerente").equals("true")) {
            listaSolicitados = agendaDao.findByStatus(Constants.SOLICITADO);
            listaAtendidos = agendaDao.findByStatus(Constants.ATENDIDO);
            listaRejeitados = agendaDao.findByStatus(Constants.REJEITADO);
            listaFinalizados = agendaDao.findByStatus(Constants.TERMINADO);

        } else if (request.getSession().getAttribute("isProfessor").equals("true")) {
            listaSolicitados = agendaDao.findByStatusProfessor(Constants.SOLICITADO, (Professor) request.getSession().getAttribute("user"));
            listaAtendidos = agendaDao.findByStatusProfessor(Constants.ATENDIDO, (Professor) request.getSession().getAttribute("user"));
            listaRejeitados = agendaDao.findByStatusProfessor(Constants.REJEITADO, (Professor) request.getSession().getAttribute("user"));
            listaFinalizados = agendaDao.findByStatusProfessor(Constants.TERMINADO, (Professor) request.getSession().getAttribute("user"));
        }
        request.setAttribute("listaSolicitados", listaSolicitados);
        request.setAttribute("listaAtendidas", listaAtendidos);
        request.setAttribute("listaRecusados", listaRejeitados);
        request.setAttribute("listaFinalizados", listaFinalizados);

        return "listarReservas.jsp";
    }

}
