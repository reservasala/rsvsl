/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.command;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reservaSala.dao.SalaJpaController;
import reservaSala.model.bean.Sala;
import reservaSala.model.connection.ConnectionFactory;

/**
 *
 * @author Jorge
 */
public class CreateSala implements InterfaceCommand {

    String campus, predio;
    int numero;
    EntityManagerFactory factory;
    private SalaJpaController salaJpaController;
    private Sala sala;
    
    
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        if(((request.getParameter("numero").equals(""))&&(request.getParameter("predio").equals(""))&&(request.getParameter("campus").equals("")))||((request.getParameter("numero").equals(""))&&(request.getParameter("predio").equals("")))||((request.getParameter("numero").equals(""))&&(request.getParameter("campus").equals("")))||((request.getParameter("campus").equals(""))&&(request.getParameter("predio").equals("")))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, preencha os dados obrigatorios!");
            request.setAttribute("mensage", "Por favor, preencha os dados obrigatorios!");
            return "cadastraSala.jsp";           
        }else if (request.getParameter("numero").equals("")) {
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um número para o Sala!");
            request.setAttribute("mensage", "Por favor, insira um número para o Sala!");
            return "cadastraSala.jsp";
        } else if((request.getParameter("predio").equals(""))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um prédio para o Sala!");
            request.setAttribute("mensage", "Por favor, insira um prédio para o Sala!");
            return "cadastraSala.jsp";
        } else if((request.getParameter("campus").equals(""))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um Campus para o Sala!");
            request.setAttribute("mensage", "Por favor, insira um Campus para o Sala!");
            return "cadastraSala.jsp";
        }
            
        try{
            numero = Integer.parseInt(request.getParameter("numero"));
        }catch(NumberFormatException ex){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um Número valido para o Sala!");
            request.setAttribute("mensage", "Por favor, insira um Número valido para o Sala!");
            return "cadastraSala.jsp";
        }
                
        sala = new Sala();
        factory = ConnectionFactory.getEntityManagerFactory();
        salaJpaController = new SalaJpaController(factory);
      
        predio = request.getParameter("predio");
        campus = request.getParameter("campus");
        
        sala.setNumero(numero);
        sala.setPredio(predio);
        sala.setCampus(campus);
        
        salaJpaController.create(sala);
        
        request.setAttribute("msg", true);
        request.setAttribute("msg_txt", "Cadastro Realizado com Sucesso!");
        request.setAttribute("mensage", "Cadastro Realizado com Sucesso!");
        
        return "home.jsp";
        
        
    }
}
