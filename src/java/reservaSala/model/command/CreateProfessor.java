/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.model.command;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import reservaSala.dao.ProfessorJpaController;
import reservaSala.model.bean.Gerente;
import reservaSala.model.bean.Professor;
import reservaSala.model.connection.ConnectionFactory;

/**
 *
 * @author Jorge
 */
public class CreateProfessor implements InterfaceCommand {

    String endereco, cpf, nome;
    EntityManagerFactory factory;
    private ProfessorJpaController professorDao;
    private Professor professor;
    
    
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        if(((request.getParameter("name").equals(""))&&(request.getParameter("cpf").equals(""))&&(request.getParameter("endereco").equals("")))||((request.getParameter("name").equals(""))&&(request.getParameter("cpf").equals("")))||((request.getParameter("name").equals(""))&&(request.getParameter("endereco").equals("")))||((request.getParameter("endereco").equals(""))&&(request.getParameter("cpf").equals("")))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, preencha os dados obrigatorios!");
            request.setAttribute("mensage", "Por favor, preencha os dados obrigatorios!");
            return "cadastraProfessor.jsp";           
        }else if (request.getParameter("name").equals("")) {
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um nome para o usuario!");
            request.setAttribute("mensage", "Por favor, insira um nome para o usuario!");
            return "cadastraProfessor.jsp";
        } else if((request.getParameter("cpf").equals(""))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um CPF para o usuario!");
            request.setAttribute("mensage", "Por favor, insira um CPF para o usuario!");
            return "cadastraProfessor.jsp";
        } else if((request.getParameter("endereco").equals(""))){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um endereço para o usuario!");
            request.setAttribute("mensage", "Por favor, insira um endereço para o usuario!");
            return "cadastraProfessor.jsp";
        }
        
        double numero;
        try{
            numero = Double.parseDouble(request.getParameter("cpf"));
        }catch(NumberFormatException ex){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um CPF valido para o usuario!");
            request.setAttribute("mensage", "Por favor, insira um CPF valido para o usuario!");
            return "cadastraProfessor.jsp";
        }

        if(request.getParameter("cpf").length()>11){
            request.setAttribute("msg", true);
            request.setAttribute("msg_txt", "Por favor, insira um CPF valido para o usuario!");
            request.setAttribute("mensage", "Por favor, insira um CPF valido para o usuario!");
            return "cadastraProfessor.jsp";
        }
                
        professor = new Professor();
        factory = ConnectionFactory.getEntityManagerFactory();
        professorDao = new ProfessorJpaController(factory);
        
        nome = request.getParameter("name");
        cpf = request.getParameter("cpf");
        endereco = request.getParameter("endereco");
        
        professor.setNome(nome);
        professor.setCpf(cpf);
        professor.setEndereco(endereco);
        professor.setGerenteCadastro((Gerente)request.getSession().getAttribute("user"));
        
        
        
        professorDao.create(professor);
        
        request.setAttribute("msg", true);
        request.setAttribute("msg_txt", "Cadastro Realizado com Sucesso!");
        request.setAttribute("mensage", "Cadastro Realizado com Sucesso!");
        
        return "home.jsp";
        
        
    }
}
