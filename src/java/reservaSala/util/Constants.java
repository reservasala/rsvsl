/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.util;

/**
 *
 * @author Admin
 */
public class Constants {

    private Constants() {
    }
    public static final Character SOLICITADO = 'C';
    public static final Character ATENDIDO = 'A';
    public static final Character TERMINADO = 'T';
    public static final Character REJEITADO = 'R';
    
}
