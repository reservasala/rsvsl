/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.model.bean.Professor;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import reservaSala.dao.exceptions.IllegalOrphanException;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Gerente;

/**
 *
 * @author Jorge
 */
public class GerenteJpaController implements Serializable {

    public GerenteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Gerente gerente) {
        if (gerente.getProfessorList() == null) {
            gerente.setProfessorList(new ArrayList<Professor>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Professor> attachedProfessorList = new ArrayList<Professor>();
            for (Professor professorListProfessorToAttach : gerente.getProfessorList()) {
                professorListProfessorToAttach = em.getReference(professorListProfessorToAttach.getClass(), professorListProfessorToAttach.getProfessorId());
                attachedProfessorList.add(professorListProfessorToAttach);
            }
            gerente.setProfessorList(attachedProfessorList);
            em.persist(gerente);
            for (Professor professorListProfessor : gerente.getProfessorList()) {
                Gerente oldGerenteCadastroOfProfessorListProfessor = professorListProfessor.getGerenteCadastro();
                professorListProfessor.setGerenteCadastro(gerente);
                professorListProfessor = em.merge(professorListProfessor);
                if (oldGerenteCadastroOfProfessorListProfessor != null) {
                    oldGerenteCadastroOfProfessorListProfessor.getProfessorList().remove(professorListProfessor);
                    oldGerenteCadastroOfProfessorListProfessor = em.merge(oldGerenteCadastroOfProfessorListProfessor);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Gerente gerente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gerente persistentGerente = em.find(Gerente.class, gerente.getGerenteId());
            List<Professor> professorListOld = persistentGerente.getProfessorList();
            List<Professor> professorListNew = gerente.getProfessorList();
            List<String> illegalOrphanMessages = null;
            for (Professor professorListOldProfessor : professorListOld) {
                if (!professorListNew.contains(professorListOldProfessor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Professor " + professorListOldProfessor + " since its gerenteCadastro field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Professor> attachedProfessorListNew = new ArrayList<Professor>();
            for (Professor professorListNewProfessorToAttach : professorListNew) {
                professorListNewProfessorToAttach = em.getReference(professorListNewProfessorToAttach.getClass(), professorListNewProfessorToAttach.getProfessorId());
                attachedProfessorListNew.add(professorListNewProfessorToAttach);
            }
            professorListNew = attachedProfessorListNew;
            gerente.setProfessorList(professorListNew);
            gerente = em.merge(gerente);
            for (Professor professorListNewProfessor : professorListNew) {
                if (!professorListOld.contains(professorListNewProfessor)) {
                    Gerente oldGerenteCadastroOfProfessorListNewProfessor = professorListNewProfessor.getGerenteCadastro();
                    professorListNewProfessor.setGerenteCadastro(gerente);
                    professorListNewProfessor = em.merge(professorListNewProfessor);
                    if (oldGerenteCadastroOfProfessorListNewProfessor != null && !oldGerenteCadastroOfProfessorListNewProfessor.equals(gerente)) {
                        oldGerenteCadastroOfProfessorListNewProfessor.getProfessorList().remove(professorListNewProfessor);
                        oldGerenteCadastroOfProfessorListNewProfessor = em.merge(oldGerenteCadastroOfProfessorListNewProfessor);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = gerente.getGerenteId();
                if (findGerente(id) == null) {
                    throw new NonexistentEntityException("The gerente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gerente gerente;
            try {
                gerente = em.getReference(Gerente.class, id);
                gerente.getGerenteId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The gerente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Professor> professorListOrphanCheck = gerente.getProfessorList();
            for (Professor professorListOrphanCheckProfessor : professorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Gerente (" + gerente + ") cannot be destroyed since the Professor " + professorListOrphanCheckProfessor + " in its professorList field has a non-nullable gerenteCadastro field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(gerente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Gerente> findGerenteEntities() {
        return findGerenteEntities(true, -1, -1);
    }

    public List<Gerente> findGerenteEntities(int maxResults, int firstResult) {
        return findGerenteEntities(false, maxResults, firstResult);
    }

    private List<Gerente> findGerenteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Gerente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Gerente findGerente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Gerente.class, id);
        } finally {
            em.close();
        }
    }

    public int getGerenteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Gerente> rt = cq.from(Gerente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
 public Gerente findGerenteByLogin(String nome) {
        Gerente gerente = null;
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("Gerente.findByNome");
        query.setParameter("nome", nome);
        try {
            gerente = (Gerente) query.getSingleResult();
        } 
        catch (NoResultException nre) {
        }
        return gerente;
    }
}
