/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Assistente;
import reservaSala.model.bean.DefeitoEquipamento;
import reservaSala.model.bean.Equipamento;

/**
 *
 * @author Jorge
 */
public class DefeitoEquipamentoJpaController implements Serializable {

    public DefeitoEquipamentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DefeitoEquipamento defeitoEquipamento) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Assistente assistente = defeitoEquipamento.getAssistente();
            if (assistente != null) {
                assistente = em.getReference(assistente.getClass(), assistente.getAssistenteId());
                defeitoEquipamento.setAssistente(assistente);
            }
            Equipamento equipamento = defeitoEquipamento.getEquipamento();
            if (equipamento != null) {
                equipamento = em.getReference(equipamento.getClass(), equipamento.getEquipamentoId());
                defeitoEquipamento.setEquipamento(equipamento);
            }
            em.persist(defeitoEquipamento);
            if (assistente != null) {
                assistente.getDefeitoEquipamentoList().add(defeitoEquipamento);
                assistente = em.merge(assistente);
            }
            if (equipamento != null) {
                equipamento.getDefeitoEquipamentoList().add(defeitoEquipamento);
                equipamento = em.merge(equipamento);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DefeitoEquipamento defeitoEquipamento) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DefeitoEquipamento persistentDefeitoEquipamento = em.find(DefeitoEquipamento.class, defeitoEquipamento.getDefeitoId());
            Assistente assistenteOld = persistentDefeitoEquipamento.getAssistente();
            Assistente assistenteNew = defeitoEquipamento.getAssistente();
            Equipamento equipamentoOld = persistentDefeitoEquipamento.getEquipamento();
            Equipamento equipamentoNew = defeitoEquipamento.getEquipamento();
            if (assistenteNew != null) {
                assistenteNew = em.getReference(assistenteNew.getClass(), assistenteNew.getAssistenteId());
                defeitoEquipamento.setAssistente(assistenteNew);
            }
            if (equipamentoNew != null) {
                equipamentoNew = em.getReference(equipamentoNew.getClass(), equipamentoNew.getEquipamentoId());
                defeitoEquipamento.setEquipamento(equipamentoNew);
            }
            defeitoEquipamento = em.merge(defeitoEquipamento);
            if (assistenteOld != null && !assistenteOld.equals(assistenteNew)) {
                assistenteOld.getDefeitoEquipamentoList().remove(defeitoEquipamento);
                assistenteOld = em.merge(assistenteOld);
            }
            if (assistenteNew != null && !assistenteNew.equals(assistenteOld)) {
                assistenteNew.getDefeitoEquipamentoList().add(defeitoEquipamento);
                assistenteNew = em.merge(assistenteNew);
            }
            if (equipamentoOld != null && !equipamentoOld.equals(equipamentoNew)) {
                equipamentoOld.getDefeitoEquipamentoList().remove(defeitoEquipamento);
                equipamentoOld = em.merge(equipamentoOld);
            }
            if (equipamentoNew != null && !equipamentoNew.equals(equipamentoOld)) {
                equipamentoNew.getDefeitoEquipamentoList().add(defeitoEquipamento);
                equipamentoNew = em.merge(equipamentoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = defeitoEquipamento.getDefeitoId();
                if (findDefeitoEquipamento(id) == null) {
                    throw new NonexistentEntityException("The defeitoEquipamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DefeitoEquipamento defeitoEquipamento;
            try {
                defeitoEquipamento = em.getReference(DefeitoEquipamento.class, id);
                defeitoEquipamento.getDefeitoId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The defeitoEquipamento with id " + id + " no longer exists.", enfe);
            }
            Assistente assistente = defeitoEquipamento.getAssistente();
            if (assistente != null) {
                assistente.getDefeitoEquipamentoList().remove(defeitoEquipamento);
                assistente = em.merge(assistente);
            }
            Equipamento equipamento = defeitoEquipamento.getEquipamento();
            if (equipamento != null) {
                equipamento.getDefeitoEquipamentoList().remove(defeitoEquipamento);
                equipamento = em.merge(equipamento);
            }
            em.remove(defeitoEquipamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DefeitoEquipamento> findDefeitoEquipamentoEntities() {
        return findDefeitoEquipamentoEntities(true, -1, -1);
    }

    public List<DefeitoEquipamento> findDefeitoEquipamentoEntities(int maxResults, int firstResult) {
        return findDefeitoEquipamentoEntities(false, maxResults, firstResult);
    }

    private List<DefeitoEquipamento> findDefeitoEquipamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DefeitoEquipamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DefeitoEquipamento findDefeitoEquipamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DefeitoEquipamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getDefeitoEquipamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DefeitoEquipamento> rt = cq.from(DefeitoEquipamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
