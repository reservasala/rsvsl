/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.model.bean.LogSala;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import reservaSala.dao.exceptions.IllegalOrphanException;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Agenda;
import reservaSala.model.bean.Sala;

/**
 *
 * @author Jorge
 */
public class SalaJpaController implements Serializable {

    public SalaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sala sala) {
        if (sala.getLogSalaList() == null) {
            sala.setLogSalaList(new ArrayList<LogSala>());
        }
        if (sala.getAgendaList() == null) {
            sala.setAgendaList(new ArrayList<Agenda>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<LogSala> attachedLogSalaList = new ArrayList<LogSala>();
            for (LogSala logSalaListLogSalaToAttach : sala.getLogSalaList()) {
                logSalaListLogSalaToAttach = em.getReference(logSalaListLogSalaToAttach.getClass(), logSalaListLogSalaToAttach.getLogSalaId());
                attachedLogSalaList.add(logSalaListLogSalaToAttach);
            }
            sala.setLogSalaList(attachedLogSalaList);
            List<Agenda> attachedAgendaList = new ArrayList<Agenda>();
            for (Agenda agendaListAgendaToAttach : sala.getAgendaList()) {
                agendaListAgendaToAttach = em.getReference(agendaListAgendaToAttach.getClass(), agendaListAgendaToAttach.getAgendaId());
                attachedAgendaList.add(agendaListAgendaToAttach);
            }
            sala.setAgendaList(attachedAgendaList);
            em.persist(sala);
            for (LogSala logSalaListLogSala : sala.getLogSalaList()) {
                Sala oldSalaOfLogSalaListLogSala = logSalaListLogSala.getSala();
                logSalaListLogSala.setSala(sala);
                logSalaListLogSala = em.merge(logSalaListLogSala);
                if (oldSalaOfLogSalaListLogSala != null) {
                    oldSalaOfLogSalaListLogSala.getLogSalaList().remove(logSalaListLogSala);
                    oldSalaOfLogSalaListLogSala = em.merge(oldSalaOfLogSalaListLogSala);
                }
            }
            for (Agenda agendaListAgenda : sala.getAgendaList()) {
                Sala oldSalaOfAgendaListAgenda = agendaListAgenda.getSala();
                agendaListAgenda.setSala(sala);
                agendaListAgenda = em.merge(agendaListAgenda);
                if (oldSalaOfAgendaListAgenda != null) {
                    oldSalaOfAgendaListAgenda.getAgendaList().remove(agendaListAgenda);
                    oldSalaOfAgendaListAgenda = em.merge(oldSalaOfAgendaListAgenda);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sala sala) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sala persistentSala = em.find(Sala.class, sala.getSalaId());
            List<LogSala> logSalaListOld = persistentSala.getLogSalaList();
            List<LogSala> logSalaListNew = sala.getLogSalaList();
            List<Agenda> agendaListOld = persistentSala.getAgendaList();
            List<Agenda> agendaListNew = sala.getAgendaList();
            List<String> illegalOrphanMessages = null;
            for (LogSala logSalaListOldLogSala : logSalaListOld) {
                if (!logSalaListNew.contains(logSalaListOldLogSala)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LogSala " + logSalaListOldLogSala + " since its sala field is not nullable.");
                }
            }
            for (Agenda agendaListOldAgenda : agendaListOld) {
                if (!agendaListNew.contains(agendaListOldAgenda)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Agenda " + agendaListOldAgenda + " since its sala field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<LogSala> attachedLogSalaListNew = new ArrayList<LogSala>();
            for (LogSala logSalaListNewLogSalaToAttach : logSalaListNew) {
                logSalaListNewLogSalaToAttach = em.getReference(logSalaListNewLogSalaToAttach.getClass(), logSalaListNewLogSalaToAttach.getLogSalaId());
                attachedLogSalaListNew.add(logSalaListNewLogSalaToAttach);
            }
            logSalaListNew = attachedLogSalaListNew;
            sala.setLogSalaList(logSalaListNew);
            List<Agenda> attachedAgendaListNew = new ArrayList<Agenda>();
            for (Agenda agendaListNewAgendaToAttach : agendaListNew) {
                agendaListNewAgendaToAttach = em.getReference(agendaListNewAgendaToAttach.getClass(), agendaListNewAgendaToAttach.getAgendaId());
                attachedAgendaListNew.add(agendaListNewAgendaToAttach);
            }
            agendaListNew = attachedAgendaListNew;
            sala.setAgendaList(agendaListNew);
            sala = em.merge(sala);
            for (LogSala logSalaListNewLogSala : logSalaListNew) {
                if (!logSalaListOld.contains(logSalaListNewLogSala)) {
                    Sala oldSalaOfLogSalaListNewLogSala = logSalaListNewLogSala.getSala();
                    logSalaListNewLogSala.setSala(sala);
                    logSalaListNewLogSala = em.merge(logSalaListNewLogSala);
                    if (oldSalaOfLogSalaListNewLogSala != null && !oldSalaOfLogSalaListNewLogSala.equals(sala)) {
                        oldSalaOfLogSalaListNewLogSala.getLogSalaList().remove(logSalaListNewLogSala);
                        oldSalaOfLogSalaListNewLogSala = em.merge(oldSalaOfLogSalaListNewLogSala);
                    }
                }
            }
            for (Agenda agendaListNewAgenda : agendaListNew) {
                if (!agendaListOld.contains(agendaListNewAgenda)) {
                    Sala oldSalaOfAgendaListNewAgenda = agendaListNewAgenda.getSala();
                    agendaListNewAgenda.setSala(sala);
                    agendaListNewAgenda = em.merge(agendaListNewAgenda);
                    if (oldSalaOfAgendaListNewAgenda != null && !oldSalaOfAgendaListNewAgenda.equals(sala)) {
                        oldSalaOfAgendaListNewAgenda.getAgendaList().remove(agendaListNewAgenda);
                        oldSalaOfAgendaListNewAgenda = em.merge(oldSalaOfAgendaListNewAgenda);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sala.getSalaId();
                if (findSala(id) == null) {
                    throw new NonexistentEntityException("The sala with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sala sala;
            try {
                sala = em.getReference(Sala.class, id);
                sala.getSalaId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sala with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<LogSala> logSalaListOrphanCheck = sala.getLogSalaList();
            for (LogSala logSalaListOrphanCheckLogSala : logSalaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sala (" + sala + ") cannot be destroyed since the LogSala " + logSalaListOrphanCheckLogSala + " in its logSalaList field has a non-nullable sala field.");
            }
            List<Agenda> agendaListOrphanCheck = sala.getAgendaList();
            for (Agenda agendaListOrphanCheckAgenda : agendaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sala (" + sala + ") cannot be destroyed since the Agenda " + agendaListOrphanCheckAgenda + " in its agendaList field has a non-nullable sala field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(sala);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sala> findSalaEntities() {
        return findSalaEntities(true, -1, -1);
    }

    public List<Sala> findSalaEntities(int maxResults, int firstResult) {
        return findSalaEntities(false, maxResults, firstResult);
    }

    public Sala findSalaByNumero(int numero) {
        Sala sala = null;
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("Sala.findByNumero");
        query.setParameter("numero", numero);
        try {
            sala = (Sala) query.getSingleResult();
        } 
        catch (NoResultException nre) {
        }
        return sala;
        }
    private List<Sala> findSalaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sala.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sala findSala(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sala.class, id);
        } finally {
            em.close();
        }
    }

    public int getSalaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sala> rt = cq.from(Sala.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
