/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Assistente;
import reservaSala.model.bean.Equipamento;
import reservaSala.model.bean.LogEquipamento;
import reservaSala.model.bean.Professor;

/**
 *
 * @author Jorge
 */
public class LogEquipamentoJpaController implements Serializable {

    public LogEquipamentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LogEquipamento logEquipamento) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Assistente assistente = logEquipamento.getAssistente();
            if (assistente != null) {
                assistente = em.getReference(assistente.getClass(), assistente.getAssistenteId());
                logEquipamento.setAssistente(assistente);
            }
            Equipamento equipamento = logEquipamento.getEquipamento();
            if (equipamento != null) {
                equipamento = em.getReference(equipamento.getClass(), equipamento.getEquipamentoId());
                logEquipamento.setEquipamento(equipamento);
            }
            Professor professor = logEquipamento.getProfessor();
            if (professor != null) {
                professor = em.getReference(professor.getClass(), professor.getProfessorId());
                logEquipamento.setProfessor(professor);
            }
            em.persist(logEquipamento);
            if (assistente != null) {
                assistente.getLogEquipamentoList().add(logEquipamento);
                assistente = em.merge(assistente);
            }
            if (equipamento != null) {
                equipamento.getLogEquipamentoList().add(logEquipamento);
                equipamento = em.merge(equipamento);
            }
            if (professor != null) {
                professor.getLogEquipamentoList().add(logEquipamento);
                professor = em.merge(professor);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LogEquipamento logEquipamento) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LogEquipamento persistentLogEquipamento = em.find(LogEquipamento.class, logEquipamento.getLogEquipamentoId());
            Assistente assistenteOld = persistentLogEquipamento.getAssistente();
            Assistente assistenteNew = logEquipamento.getAssistente();
            Equipamento equipamentoOld = persistentLogEquipamento.getEquipamento();
            Equipamento equipamentoNew = logEquipamento.getEquipamento();
            Professor professorOld = persistentLogEquipamento.getProfessor();
            Professor professorNew = logEquipamento.getProfessor();
            if (assistenteNew != null) {
                assistenteNew = em.getReference(assistenteNew.getClass(), assistenteNew.getAssistenteId());
                logEquipamento.setAssistente(assistenteNew);
            }
            if (equipamentoNew != null) {
                equipamentoNew = em.getReference(equipamentoNew.getClass(), equipamentoNew.getEquipamentoId());
                logEquipamento.setEquipamento(equipamentoNew);
            }
            if (professorNew != null) {
                professorNew = em.getReference(professorNew.getClass(), professorNew.getProfessorId());
                logEquipamento.setProfessor(professorNew);
            }
            logEquipamento = em.merge(logEquipamento);
            if (assistenteOld != null && !assistenteOld.equals(assistenteNew)) {
                assistenteOld.getLogEquipamentoList().remove(logEquipamento);
                assistenteOld = em.merge(assistenteOld);
            }
            if (assistenteNew != null && !assistenteNew.equals(assistenteOld)) {
                assistenteNew.getLogEquipamentoList().add(logEquipamento);
                assistenteNew = em.merge(assistenteNew);
            }
            if (equipamentoOld != null && !equipamentoOld.equals(equipamentoNew)) {
                equipamentoOld.getLogEquipamentoList().remove(logEquipamento);
                equipamentoOld = em.merge(equipamentoOld);
            }
            if (equipamentoNew != null && !equipamentoNew.equals(equipamentoOld)) {
                equipamentoNew.getLogEquipamentoList().add(logEquipamento);
                equipamentoNew = em.merge(equipamentoNew);
            }
            if (professorOld != null && !professorOld.equals(professorNew)) {
                professorOld.getLogEquipamentoList().remove(logEquipamento);
                professorOld = em.merge(professorOld);
            }
            if (professorNew != null && !professorNew.equals(professorOld)) {
                professorNew.getLogEquipamentoList().add(logEquipamento);
                professorNew = em.merge(professorNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = logEquipamento.getLogEquipamentoId();
                if (findLogEquipamento(id) == null) {
                    throw new NonexistentEntityException("The logEquipamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LogEquipamento logEquipamento;
            try {
                logEquipamento = em.getReference(LogEquipamento.class, id);
                logEquipamento.getLogEquipamentoId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The logEquipamento with id " + id + " no longer exists.", enfe);
            }
            Assistente assistente = logEquipamento.getAssistente();
            if (assistente != null) {
                assistente.getLogEquipamentoList().remove(logEquipamento);
                assistente = em.merge(assistente);
            }
            Equipamento equipamento = logEquipamento.getEquipamento();
            if (equipamento != null) {
                equipamento.getLogEquipamentoList().remove(logEquipamento);
                equipamento = em.merge(equipamento);
            }
            Professor professor = logEquipamento.getProfessor();
            if (professor != null) {
                professor.getLogEquipamentoList().remove(logEquipamento);
                professor = em.merge(professor);
            }
            em.remove(logEquipamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LogEquipamento> findLogEquipamentoEntities() {
        return findLogEquipamentoEntities(true, -1, -1);
    }

    public List<LogEquipamento> findLogEquipamentoEntities(int maxResults, int firstResult) {
        return findLogEquipamentoEntities(false, maxResults, firstResult);
    }

    private List<LogEquipamento> findLogEquipamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LogEquipamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LogEquipamento findLogEquipamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LogEquipamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getLogEquipamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LogEquipamento> rt = cq.from(LogEquipamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
