/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Agenda;
import reservaSala.model.bean.Equipamento;
import reservaSala.model.bean.Professor;
import reservaSala.model.bean.Sala;

/**
 *
 * @author Jorge
 */
public class AgendaJpaController implements Serializable {

    public AgendaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Agenda agenda) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Equipamento equipamento = agenda.getEquipamento();
            if (equipamento != null) {
                equipamento = em.getReference(equipamento.getClass(), equipamento.getEquipamentoId());
                agenda.setEquipamento(equipamento);
            }
            Professor professor = agenda.getProfessor();
            if (professor != null) {
                professor = em.getReference(professor.getClass(), professor.getProfessorId());
                agenda.setProfessor(professor);
            }
            Sala sala = agenda.getSala();
            if (sala != null) {
                sala = em.getReference(sala.getClass(), sala.getSalaId());
                agenda.setSala(sala);
            }
            em.persist(agenda);
            if (equipamento != null) {
                equipamento.getAgendaList().add(agenda);
                equipamento = em.merge(equipamento);
            }
            if (professor != null) {
                professor.getAgendaList().add(agenda);
                professor = em.merge(professor);
            }
            if (sala != null) {
                sala.getAgendaList().add(agenda);
                sala = em.merge(sala);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Agenda agenda) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Agenda persistentAgenda = em.find(Agenda.class, agenda.getAgendaId());
            Equipamento equipamentoOld = persistentAgenda.getEquipamento();
            Equipamento equipamentoNew = agenda.getEquipamento();
            Professor professorOld = persistentAgenda.getProfessor();
            Professor professorNew = agenda.getProfessor();
            Sala salaOld = persistentAgenda.getSala();
            Sala salaNew = agenda.getSala();
            if (equipamentoNew != null) {
                equipamentoNew = em.getReference(equipamentoNew.getClass(), equipamentoNew.getEquipamentoId());
                agenda.setEquipamento(equipamentoNew);
            }
            if (professorNew != null) {
                professorNew = em.getReference(professorNew.getClass(), professorNew.getProfessorId());
                agenda.setProfessor(professorNew);
            }
            if (salaNew != null) {
                salaNew = em.getReference(salaNew.getClass(), salaNew.getSalaId());
                agenda.setSala(salaNew);
            }
            agenda = em.merge(agenda);
            if (equipamentoOld != null && !equipamentoOld.equals(equipamentoNew)) {
                equipamentoOld.getAgendaList().remove(agenda);
                equipamentoOld = em.merge(equipamentoOld);
            }
            if (equipamentoNew != null && !equipamentoNew.equals(equipamentoOld)) {
                equipamentoNew.getAgendaList().add(agenda);
                equipamentoNew = em.merge(equipamentoNew);
            }
            if (professorOld != null && !professorOld.equals(professorNew)) {
                professorOld.getAgendaList().remove(agenda);
                professorOld = em.merge(professorOld);
            }
            if (professorNew != null && !professorNew.equals(professorOld)) {
                professorNew.getAgendaList().add(agenda);
                professorNew = em.merge(professorNew);
            }
            if (salaOld != null && !salaOld.equals(salaNew)) {
                salaOld.getAgendaList().remove(agenda);
                salaOld = em.merge(salaOld);
            }
            if (salaNew != null && !salaNew.equals(salaOld)) {
                salaNew.getAgendaList().add(agenda);
                salaNew = em.merge(salaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = agenda.getAgendaId();
                if (findAgenda(id) == null) {
                    throw new NonexistentEntityException("The agenda with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Agenda agenda;
            try {
                agenda = em.getReference(Agenda.class, id);
                agenda.getAgendaId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The agenda with id " + id + " no longer exists.", enfe);
            }
            Equipamento equipamento = agenda.getEquipamento();
            if (equipamento != null) {
                equipamento.getAgendaList().remove(agenda);
                equipamento = em.merge(equipamento);
            }
            Professor professor = agenda.getProfessor();
            if (professor != null) {
                professor.getAgendaList().remove(agenda);
                professor = em.merge(professor);
            }
            Sala sala = agenda.getSala();
            if (sala != null) {
                sala.getAgendaList().remove(agenda);
                sala = em.merge(sala);
            }
            em.remove(agenda);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Agenda> findAgendaEntities() {
        return findAgendaEntities(true, -1, -1);
    }

    public List<Agenda> findAgendaEntities(int maxResults, int firstResult) {
        return findAgendaEntities(false, maxResults, firstResult);
    }

    private List<Agenda> findAgendaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Agenda.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Agenda findAgenda(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Agenda.class, id);
        } finally {
            em.close();
        }
    }

    public int getAgendaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Agenda> rt = cq.from(Agenda.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Agenda> findByStatus(Character status) {

        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("Agenda.findByStatus");
        query.setParameter("status", status);
        try {
            return query.getResultList();
        }
        catch (NoResultException nre) {
        }
        return null;
    }
        public List<Agenda> findByStatusProfessor(Character status, Professor p) {

        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("Agenda.findByStatusProfessor");
        query.setParameter("status", status);
        query.setParameter("professor", p);
        try {
            return query.getResultList();
        }
        catch (NoResultException nre) {
        }
        return null;
    }
    
    
}
