/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.model.bean.LogEquipamento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import reservaSala.dao.exceptions.IllegalOrphanException;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Agenda;
import reservaSala.model.bean.DefeitoEquipamento;
import reservaSala.model.bean.Equipamento;

/**
 *
 * @author Jorge
 */
public class EquipamentoJpaController implements Serializable {

    public EquipamentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Equipamento equipamento) {
        if (equipamento.getLogEquipamentoList() == null) {
            equipamento.setLogEquipamentoList(new ArrayList<LogEquipamento>());
        }
        if (equipamento.getAgendaList() == null) {
            equipamento.setAgendaList(new ArrayList<Agenda>());
        }
        if (equipamento.getDefeitoEquipamentoList() == null) {
            equipamento.setDefeitoEquipamentoList(new ArrayList<DefeitoEquipamento>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<LogEquipamento> attachedLogEquipamentoList = new ArrayList<LogEquipamento>();
            for (LogEquipamento logEquipamentoListLogEquipamentoToAttach : equipamento.getLogEquipamentoList()) {
                logEquipamentoListLogEquipamentoToAttach = em.getReference(logEquipamentoListLogEquipamentoToAttach.getClass(), logEquipamentoListLogEquipamentoToAttach.getLogEquipamentoId());
                attachedLogEquipamentoList.add(logEquipamentoListLogEquipamentoToAttach);
            }
            equipamento.setLogEquipamentoList(attachedLogEquipamentoList);
            List<Agenda> attachedAgendaList = new ArrayList<Agenda>();
            for (Agenda agendaListAgendaToAttach : equipamento.getAgendaList()) {
                agendaListAgendaToAttach = em.getReference(agendaListAgendaToAttach.getClass(), agendaListAgendaToAttach.getAgendaId());
                attachedAgendaList.add(agendaListAgendaToAttach);
            }
            equipamento.setAgendaList(attachedAgendaList);
            List<DefeitoEquipamento> attachedDefeitoEquipamentoList = new ArrayList<DefeitoEquipamento>();
            for (DefeitoEquipamento defeitoEquipamentoListDefeitoEquipamentoToAttach : equipamento.getDefeitoEquipamentoList()) {
                defeitoEquipamentoListDefeitoEquipamentoToAttach = em.getReference(defeitoEquipamentoListDefeitoEquipamentoToAttach.getClass(), defeitoEquipamentoListDefeitoEquipamentoToAttach.getDefeitoId());
                attachedDefeitoEquipamentoList.add(defeitoEquipamentoListDefeitoEquipamentoToAttach);
            }
            equipamento.setDefeitoEquipamentoList(attachedDefeitoEquipamentoList);
            em.persist(equipamento);
            for (LogEquipamento logEquipamentoListLogEquipamento : equipamento.getLogEquipamentoList()) {
                Equipamento oldEquipamentoOfLogEquipamentoListLogEquipamento = logEquipamentoListLogEquipamento.getEquipamento();
                logEquipamentoListLogEquipamento.setEquipamento(equipamento);
                logEquipamentoListLogEquipamento = em.merge(logEquipamentoListLogEquipamento);
                if (oldEquipamentoOfLogEquipamentoListLogEquipamento != null) {
                    oldEquipamentoOfLogEquipamentoListLogEquipamento.getLogEquipamentoList().remove(logEquipamentoListLogEquipamento);
                    oldEquipamentoOfLogEquipamentoListLogEquipamento = em.merge(oldEquipamentoOfLogEquipamentoListLogEquipamento);
                }
            }
            for (Agenda agendaListAgenda : equipamento.getAgendaList()) {
                Equipamento oldEquipamentoOfAgendaListAgenda = agendaListAgenda.getEquipamento();
                agendaListAgenda.setEquipamento(equipamento);
                agendaListAgenda = em.merge(agendaListAgenda);
                if (oldEquipamentoOfAgendaListAgenda != null) {
                    oldEquipamentoOfAgendaListAgenda.getAgendaList().remove(agendaListAgenda);
                    oldEquipamentoOfAgendaListAgenda = em.merge(oldEquipamentoOfAgendaListAgenda);
                }
            }
            for (DefeitoEquipamento defeitoEquipamentoListDefeitoEquipamento : equipamento.getDefeitoEquipamentoList()) {
                Equipamento oldEquipamentoOfDefeitoEquipamentoListDefeitoEquipamento = defeitoEquipamentoListDefeitoEquipamento.getEquipamento();
                defeitoEquipamentoListDefeitoEquipamento.setEquipamento(equipamento);
                defeitoEquipamentoListDefeitoEquipamento = em.merge(defeitoEquipamentoListDefeitoEquipamento);
                if (oldEquipamentoOfDefeitoEquipamentoListDefeitoEquipamento != null) {
                    oldEquipamentoOfDefeitoEquipamentoListDefeitoEquipamento.getDefeitoEquipamentoList().remove(defeitoEquipamentoListDefeitoEquipamento);
                    oldEquipamentoOfDefeitoEquipamentoListDefeitoEquipamento = em.merge(oldEquipamentoOfDefeitoEquipamentoListDefeitoEquipamento);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Equipamento equipamento) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Equipamento persistentEquipamento = em.find(Equipamento.class, equipamento.getEquipamentoId());
            List<LogEquipamento> logEquipamentoListOld = persistentEquipamento.getLogEquipamentoList();
            List<LogEquipamento> logEquipamentoListNew = equipamento.getLogEquipamentoList();
            List<Agenda> agendaListOld = persistentEquipamento.getAgendaList();
            List<Agenda> agendaListNew = equipamento.getAgendaList();
            List<DefeitoEquipamento> defeitoEquipamentoListOld = persistentEquipamento.getDefeitoEquipamentoList();
            List<DefeitoEquipamento> defeitoEquipamentoListNew = equipamento.getDefeitoEquipamentoList();
            List<String> illegalOrphanMessages = null;
            for (LogEquipamento logEquipamentoListOldLogEquipamento : logEquipamentoListOld) {
                if (!logEquipamentoListNew.contains(logEquipamentoListOldLogEquipamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LogEquipamento " + logEquipamentoListOldLogEquipamento + " since its equipamento field is not nullable.");
                }
            }
            for (DefeitoEquipamento defeitoEquipamentoListOldDefeitoEquipamento : defeitoEquipamentoListOld) {
                if (!defeitoEquipamentoListNew.contains(defeitoEquipamentoListOldDefeitoEquipamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DefeitoEquipamento " + defeitoEquipamentoListOldDefeitoEquipamento + " since its equipamento field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<LogEquipamento> attachedLogEquipamentoListNew = new ArrayList<LogEquipamento>();
            for (LogEquipamento logEquipamentoListNewLogEquipamentoToAttach : logEquipamentoListNew) {
                logEquipamentoListNewLogEquipamentoToAttach = em.getReference(logEquipamentoListNewLogEquipamentoToAttach.getClass(), logEquipamentoListNewLogEquipamentoToAttach.getLogEquipamentoId());
                attachedLogEquipamentoListNew.add(logEquipamentoListNewLogEquipamentoToAttach);
            }
            logEquipamentoListNew = attachedLogEquipamentoListNew;
            equipamento.setLogEquipamentoList(logEquipamentoListNew);
            List<Agenda> attachedAgendaListNew = new ArrayList<Agenda>();
            for (Agenda agendaListNewAgendaToAttach : agendaListNew) {
                agendaListNewAgendaToAttach = em.getReference(agendaListNewAgendaToAttach.getClass(), agendaListNewAgendaToAttach.getAgendaId());
                attachedAgendaListNew.add(agendaListNewAgendaToAttach);
            }
            agendaListNew = attachedAgendaListNew;
            equipamento.setAgendaList(agendaListNew);
            List<DefeitoEquipamento> attachedDefeitoEquipamentoListNew = new ArrayList<DefeitoEquipamento>();
            for (DefeitoEquipamento defeitoEquipamentoListNewDefeitoEquipamentoToAttach : defeitoEquipamentoListNew) {
                defeitoEquipamentoListNewDefeitoEquipamentoToAttach = em.getReference(defeitoEquipamentoListNewDefeitoEquipamentoToAttach.getClass(), defeitoEquipamentoListNewDefeitoEquipamentoToAttach.getDefeitoId());
                attachedDefeitoEquipamentoListNew.add(defeitoEquipamentoListNewDefeitoEquipamentoToAttach);
            }
            defeitoEquipamentoListNew = attachedDefeitoEquipamentoListNew;
            equipamento.setDefeitoEquipamentoList(defeitoEquipamentoListNew);
            equipamento = em.merge(equipamento);
            for (LogEquipamento logEquipamentoListNewLogEquipamento : logEquipamentoListNew) {
                if (!logEquipamentoListOld.contains(logEquipamentoListNewLogEquipamento)) {
                    Equipamento oldEquipamentoOfLogEquipamentoListNewLogEquipamento = logEquipamentoListNewLogEquipamento.getEquipamento();
                    logEquipamentoListNewLogEquipamento.setEquipamento(equipamento);
                    logEquipamentoListNewLogEquipamento = em.merge(logEquipamentoListNewLogEquipamento);
                    if (oldEquipamentoOfLogEquipamentoListNewLogEquipamento != null && !oldEquipamentoOfLogEquipamentoListNewLogEquipamento.equals(equipamento)) {
                        oldEquipamentoOfLogEquipamentoListNewLogEquipamento.getLogEquipamentoList().remove(logEquipamentoListNewLogEquipamento);
                        oldEquipamentoOfLogEquipamentoListNewLogEquipamento = em.merge(oldEquipamentoOfLogEquipamentoListNewLogEquipamento);
                    }
                }
            }
            for (Agenda agendaListOldAgenda : agendaListOld) {
                if (!agendaListNew.contains(agendaListOldAgenda)) {
                    agendaListOldAgenda.setEquipamento(null);
                    agendaListOldAgenda = em.merge(agendaListOldAgenda);
                }
            }
            for (Agenda agendaListNewAgenda : agendaListNew) {
                if (!agendaListOld.contains(agendaListNewAgenda)) {
                    Equipamento oldEquipamentoOfAgendaListNewAgenda = agendaListNewAgenda.getEquipamento();
                    agendaListNewAgenda.setEquipamento(equipamento);
                    agendaListNewAgenda = em.merge(agendaListNewAgenda);
                    if (oldEquipamentoOfAgendaListNewAgenda != null && !oldEquipamentoOfAgendaListNewAgenda.equals(equipamento)) {
                        oldEquipamentoOfAgendaListNewAgenda.getAgendaList().remove(agendaListNewAgenda);
                        oldEquipamentoOfAgendaListNewAgenda = em.merge(oldEquipamentoOfAgendaListNewAgenda);
                    }
                }
            }
            for (DefeitoEquipamento defeitoEquipamentoListNewDefeitoEquipamento : defeitoEquipamentoListNew) {
                if (!defeitoEquipamentoListOld.contains(defeitoEquipamentoListNewDefeitoEquipamento)) {
                    Equipamento oldEquipamentoOfDefeitoEquipamentoListNewDefeitoEquipamento = defeitoEquipamentoListNewDefeitoEquipamento.getEquipamento();
                    defeitoEquipamentoListNewDefeitoEquipamento.setEquipamento(equipamento);
                    defeitoEquipamentoListNewDefeitoEquipamento = em.merge(defeitoEquipamentoListNewDefeitoEquipamento);
                    if (oldEquipamentoOfDefeitoEquipamentoListNewDefeitoEquipamento != null && !oldEquipamentoOfDefeitoEquipamentoListNewDefeitoEquipamento.equals(equipamento)) {
                        oldEquipamentoOfDefeitoEquipamentoListNewDefeitoEquipamento.getDefeitoEquipamentoList().remove(defeitoEquipamentoListNewDefeitoEquipamento);
                        oldEquipamentoOfDefeitoEquipamentoListNewDefeitoEquipamento = em.merge(oldEquipamentoOfDefeitoEquipamentoListNewDefeitoEquipamento);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = equipamento.getEquipamentoId();
                if (findEquipamento(id) == null) {
                    throw new NonexistentEntityException("The equipamento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Equipamento equipamento;
            try {
                equipamento = em.getReference(Equipamento.class, id);
                equipamento.getEquipamentoId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The equipamento with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<LogEquipamento> logEquipamentoListOrphanCheck = equipamento.getLogEquipamentoList();
            for (LogEquipamento logEquipamentoListOrphanCheckLogEquipamento : logEquipamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Equipamento (" + equipamento + ") cannot be destroyed since the LogEquipamento " + logEquipamentoListOrphanCheckLogEquipamento + " in its logEquipamentoList field has a non-nullable equipamento field.");
            }
            List<DefeitoEquipamento> defeitoEquipamentoListOrphanCheck = equipamento.getDefeitoEquipamentoList();
            for (DefeitoEquipamento defeitoEquipamentoListOrphanCheckDefeitoEquipamento : defeitoEquipamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Equipamento (" + equipamento + ") cannot be destroyed since the DefeitoEquipamento " + defeitoEquipamentoListOrphanCheckDefeitoEquipamento + " in its defeitoEquipamentoList field has a non-nullable equipamento field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Agenda> agendaList = equipamento.getAgendaList();
            for (Agenda agendaListAgenda : agendaList) {
                agendaListAgenda.setEquipamento(null);
                agendaListAgenda = em.merge(agendaListAgenda);
            }
            em.remove(equipamento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Equipamento> findEquipamentoEntities() {
        return findEquipamentoEntities(true, -1, -1);
    }

    public List<Equipamento> findEquipamentoEntities(int maxResults, int firstResult) {
        return findEquipamentoEntities(false, maxResults, firstResult);
    }

    private List<Equipamento> findEquipamentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Equipamento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Equipamento findEquipamento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Equipamento.class, id);
        } finally {
            em.close();
        }
    }

    public int getEquipamentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Equipamento> rt = cq.from(Equipamento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
