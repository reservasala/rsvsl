/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.model.bean.LogEquipamento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import reservaSala.dao.exceptions.IllegalOrphanException;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Assistente;
import reservaSala.model.bean.LogSala;
import reservaSala.model.bean.DefeitoEquipamento;

/**
 *
 * @author Jorge
 */
public class AssistenteJpaController implements Serializable {

    public AssistenteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Assistente assistente) {
        if (assistente.getLogEquipamentoList() == null) {
            assistente.setLogEquipamentoList(new ArrayList<LogEquipamento>());
        }
        if (assistente.getLogSalaList() == null) {
            assistente.setLogSalaList(new ArrayList<LogSala>());
        }
        if (assistente.getDefeitoEquipamentoList() == null) {
            assistente.setDefeitoEquipamentoList(new ArrayList<DefeitoEquipamento>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<LogEquipamento> attachedLogEquipamentoList = new ArrayList<LogEquipamento>();
            for (LogEquipamento logEquipamentoListLogEquipamentoToAttach : assistente.getLogEquipamentoList()) {
                logEquipamentoListLogEquipamentoToAttach = em.getReference(logEquipamentoListLogEquipamentoToAttach.getClass(), logEquipamentoListLogEquipamentoToAttach.getLogEquipamentoId());
                attachedLogEquipamentoList.add(logEquipamentoListLogEquipamentoToAttach);
            }
            assistente.setLogEquipamentoList(attachedLogEquipamentoList);
            List<LogSala> attachedLogSalaList = new ArrayList<LogSala>();
            for (LogSala logSalaListLogSalaToAttach : assistente.getLogSalaList()) {
                logSalaListLogSalaToAttach = em.getReference(logSalaListLogSalaToAttach.getClass(), logSalaListLogSalaToAttach.getLogSalaId());
                attachedLogSalaList.add(logSalaListLogSalaToAttach);
            }
            assistente.setLogSalaList(attachedLogSalaList);
            List<DefeitoEquipamento> attachedDefeitoEquipamentoList = new ArrayList<DefeitoEquipamento>();
            for (DefeitoEquipamento defeitoEquipamentoListDefeitoEquipamentoToAttach : assistente.getDefeitoEquipamentoList()) {
                defeitoEquipamentoListDefeitoEquipamentoToAttach = em.getReference(defeitoEquipamentoListDefeitoEquipamentoToAttach.getClass(), defeitoEquipamentoListDefeitoEquipamentoToAttach.getDefeitoId());
                attachedDefeitoEquipamentoList.add(defeitoEquipamentoListDefeitoEquipamentoToAttach);
            }
            assistente.setDefeitoEquipamentoList(attachedDefeitoEquipamentoList);
            em.persist(assistente);
            for (LogEquipamento logEquipamentoListLogEquipamento : assistente.getLogEquipamentoList()) {
                Assistente oldAssistenteOfLogEquipamentoListLogEquipamento = logEquipamentoListLogEquipamento.getAssistente();
                logEquipamentoListLogEquipamento.setAssistente(assistente);
                logEquipamentoListLogEquipamento = em.merge(logEquipamentoListLogEquipamento);
                if (oldAssistenteOfLogEquipamentoListLogEquipamento != null) {
                    oldAssistenteOfLogEquipamentoListLogEquipamento.getLogEquipamentoList().remove(logEquipamentoListLogEquipamento);
                    oldAssistenteOfLogEquipamentoListLogEquipamento = em.merge(oldAssistenteOfLogEquipamentoListLogEquipamento);
                }
            }
            for (LogSala logSalaListLogSala : assistente.getLogSalaList()) {
                Assistente oldAssistenteOfLogSalaListLogSala = logSalaListLogSala.getAssistente();
                logSalaListLogSala.setAssistente(assistente);
                logSalaListLogSala = em.merge(logSalaListLogSala);
                if (oldAssistenteOfLogSalaListLogSala != null) {
                    oldAssistenteOfLogSalaListLogSala.getLogSalaList().remove(logSalaListLogSala);
                    oldAssistenteOfLogSalaListLogSala = em.merge(oldAssistenteOfLogSalaListLogSala);
                }
            }
            for (DefeitoEquipamento defeitoEquipamentoListDefeitoEquipamento : assistente.getDefeitoEquipamentoList()) {
                Assistente oldAssistenteOfDefeitoEquipamentoListDefeitoEquipamento = defeitoEquipamentoListDefeitoEquipamento.getAssistente();
                defeitoEquipamentoListDefeitoEquipamento.setAssistente(assistente);
                defeitoEquipamentoListDefeitoEquipamento = em.merge(defeitoEquipamentoListDefeitoEquipamento);
                if (oldAssistenteOfDefeitoEquipamentoListDefeitoEquipamento != null) {
                    oldAssistenteOfDefeitoEquipamentoListDefeitoEquipamento.getDefeitoEquipamentoList().remove(defeitoEquipamentoListDefeitoEquipamento);
                    oldAssistenteOfDefeitoEquipamentoListDefeitoEquipamento = em.merge(oldAssistenteOfDefeitoEquipamentoListDefeitoEquipamento);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Assistente assistente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Assistente persistentAssistente = em.find(Assistente.class, assistente.getAssistenteId());
            List<LogEquipamento> logEquipamentoListOld = persistentAssistente.getLogEquipamentoList();
            List<LogEquipamento> logEquipamentoListNew = assistente.getLogEquipamentoList();
            List<LogSala> logSalaListOld = persistentAssistente.getLogSalaList();
            List<LogSala> logSalaListNew = assistente.getLogSalaList();
            List<DefeitoEquipamento> defeitoEquipamentoListOld = persistentAssistente.getDefeitoEquipamentoList();
            List<DefeitoEquipamento> defeitoEquipamentoListNew = assistente.getDefeitoEquipamentoList();
            List<String> illegalOrphanMessages = null;
            for (LogEquipamento logEquipamentoListOldLogEquipamento : logEquipamentoListOld) {
                if (!logEquipamentoListNew.contains(logEquipamentoListOldLogEquipamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LogEquipamento " + logEquipamentoListOldLogEquipamento + " since its assistente field is not nullable.");
                }
            }
            for (LogSala logSalaListOldLogSala : logSalaListOld) {
                if (!logSalaListNew.contains(logSalaListOldLogSala)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LogSala " + logSalaListOldLogSala + " since its assistente field is not nullable.");
                }
            }
            for (DefeitoEquipamento defeitoEquipamentoListOldDefeitoEquipamento : defeitoEquipamentoListOld) {
                if (!defeitoEquipamentoListNew.contains(defeitoEquipamentoListOldDefeitoEquipamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DefeitoEquipamento " + defeitoEquipamentoListOldDefeitoEquipamento + " since its assistente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<LogEquipamento> attachedLogEquipamentoListNew = new ArrayList<LogEquipamento>();
            for (LogEquipamento logEquipamentoListNewLogEquipamentoToAttach : logEquipamentoListNew) {
                logEquipamentoListNewLogEquipamentoToAttach = em.getReference(logEquipamentoListNewLogEquipamentoToAttach.getClass(), logEquipamentoListNewLogEquipamentoToAttach.getLogEquipamentoId());
                attachedLogEquipamentoListNew.add(logEquipamentoListNewLogEquipamentoToAttach);
            }
            logEquipamentoListNew = attachedLogEquipamentoListNew;
            assistente.setLogEquipamentoList(logEquipamentoListNew);
            List<LogSala> attachedLogSalaListNew = new ArrayList<LogSala>();
            for (LogSala logSalaListNewLogSalaToAttach : logSalaListNew) {
                logSalaListNewLogSalaToAttach = em.getReference(logSalaListNewLogSalaToAttach.getClass(), logSalaListNewLogSalaToAttach.getLogSalaId());
                attachedLogSalaListNew.add(logSalaListNewLogSalaToAttach);
            }
            logSalaListNew = attachedLogSalaListNew;
            assistente.setLogSalaList(logSalaListNew);
            List<DefeitoEquipamento> attachedDefeitoEquipamentoListNew = new ArrayList<DefeitoEquipamento>();
            for (DefeitoEquipamento defeitoEquipamentoListNewDefeitoEquipamentoToAttach : defeitoEquipamentoListNew) {
                defeitoEquipamentoListNewDefeitoEquipamentoToAttach = em.getReference(defeitoEquipamentoListNewDefeitoEquipamentoToAttach.getClass(), defeitoEquipamentoListNewDefeitoEquipamentoToAttach.getDefeitoId());
                attachedDefeitoEquipamentoListNew.add(defeitoEquipamentoListNewDefeitoEquipamentoToAttach);
            }
            defeitoEquipamentoListNew = attachedDefeitoEquipamentoListNew;
            assistente.setDefeitoEquipamentoList(defeitoEquipamentoListNew);
            assistente = em.merge(assistente);
            for (LogEquipamento logEquipamentoListNewLogEquipamento : logEquipamentoListNew) {
                if (!logEquipamentoListOld.contains(logEquipamentoListNewLogEquipamento)) {
                    Assistente oldAssistenteOfLogEquipamentoListNewLogEquipamento = logEquipamentoListNewLogEquipamento.getAssistente();
                    logEquipamentoListNewLogEquipamento.setAssistente(assistente);
                    logEquipamentoListNewLogEquipamento = em.merge(logEquipamentoListNewLogEquipamento);
                    if (oldAssistenteOfLogEquipamentoListNewLogEquipamento != null && !oldAssistenteOfLogEquipamentoListNewLogEquipamento.equals(assistente)) {
                        oldAssistenteOfLogEquipamentoListNewLogEquipamento.getLogEquipamentoList().remove(logEquipamentoListNewLogEquipamento);
                        oldAssistenteOfLogEquipamentoListNewLogEquipamento = em.merge(oldAssistenteOfLogEquipamentoListNewLogEquipamento);
                    }
                }
            }
            for (LogSala logSalaListNewLogSala : logSalaListNew) {
                if (!logSalaListOld.contains(logSalaListNewLogSala)) {
                    Assistente oldAssistenteOfLogSalaListNewLogSala = logSalaListNewLogSala.getAssistente();
                    logSalaListNewLogSala.setAssistente(assistente);
                    logSalaListNewLogSala = em.merge(logSalaListNewLogSala);
                    if (oldAssistenteOfLogSalaListNewLogSala != null && !oldAssistenteOfLogSalaListNewLogSala.equals(assistente)) {
                        oldAssistenteOfLogSalaListNewLogSala.getLogSalaList().remove(logSalaListNewLogSala);
                        oldAssistenteOfLogSalaListNewLogSala = em.merge(oldAssistenteOfLogSalaListNewLogSala);
                    }
                }
            }
            for (DefeitoEquipamento defeitoEquipamentoListNewDefeitoEquipamento : defeitoEquipamentoListNew) {
                if (!defeitoEquipamentoListOld.contains(defeitoEquipamentoListNewDefeitoEquipamento)) {
                    Assistente oldAssistenteOfDefeitoEquipamentoListNewDefeitoEquipamento = defeitoEquipamentoListNewDefeitoEquipamento.getAssistente();
                    defeitoEquipamentoListNewDefeitoEquipamento.setAssistente(assistente);
                    defeitoEquipamentoListNewDefeitoEquipamento = em.merge(defeitoEquipamentoListNewDefeitoEquipamento);
                    if (oldAssistenteOfDefeitoEquipamentoListNewDefeitoEquipamento != null && !oldAssistenteOfDefeitoEquipamentoListNewDefeitoEquipamento.equals(assistente)) {
                        oldAssistenteOfDefeitoEquipamentoListNewDefeitoEquipamento.getDefeitoEquipamentoList().remove(defeitoEquipamentoListNewDefeitoEquipamento);
                        oldAssistenteOfDefeitoEquipamentoListNewDefeitoEquipamento = em.merge(oldAssistenteOfDefeitoEquipamentoListNewDefeitoEquipamento);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = assistente.getAssistenteId();
                if (findAssistente(id) == null) {
                    throw new NonexistentEntityException("The assistente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Assistente assistente;
            try {
                assistente = em.getReference(Assistente.class, id);
                assistente.getAssistenteId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The assistente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<LogEquipamento> logEquipamentoListOrphanCheck = assistente.getLogEquipamentoList();
            for (LogEquipamento logEquipamentoListOrphanCheckLogEquipamento : logEquipamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Assistente (" + assistente + ") cannot be destroyed since the LogEquipamento " + logEquipamentoListOrphanCheckLogEquipamento + " in its logEquipamentoList field has a non-nullable assistente field.");
            }
            List<LogSala> logSalaListOrphanCheck = assistente.getLogSalaList();
            for (LogSala logSalaListOrphanCheckLogSala : logSalaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Assistente (" + assistente + ") cannot be destroyed since the LogSala " + logSalaListOrphanCheckLogSala + " in its logSalaList field has a non-nullable assistente field.");
            }
            List<DefeitoEquipamento> defeitoEquipamentoListOrphanCheck = assistente.getDefeitoEquipamentoList();
            for (DefeitoEquipamento defeitoEquipamentoListOrphanCheckDefeitoEquipamento : defeitoEquipamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Assistente (" + assistente + ") cannot be destroyed since the DefeitoEquipamento " + defeitoEquipamentoListOrphanCheckDefeitoEquipamento + " in its defeitoEquipamentoList field has a non-nullable assistente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(assistente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Assistente> findAssistenteEntities() {
        return findAssistenteEntities(true, -1, -1);
    }

    public List<Assistente> findAssistenteEntities(int maxResults, int firstResult) {
        return findAssistenteEntities(false, maxResults, firstResult);
    }

    private List<Assistente> findAssistenteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Assistente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Assistente findAssistente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Assistente.class, id);
        } finally {
            em.close();
        }
    }

    public int getAssistenteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Assistente> rt = cq.from(Assistente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
