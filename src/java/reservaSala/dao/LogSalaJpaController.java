/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.Assistente;
import reservaSala.model.bean.LogSala;
import reservaSala.model.bean.Professor;
import reservaSala.model.bean.Sala;

/**
 *
 * @author Jorge
 */
public class LogSalaJpaController implements Serializable {

    public LogSalaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LogSala logSala) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Assistente assistente = logSala.getAssistente();
            if (assistente != null) {
                assistente = em.getReference(assistente.getClass(), assistente.getAssistenteId());
                logSala.setAssistente(assistente);
            }
            Professor professor = logSala.getProfessor();
            if (professor != null) {
                professor = em.getReference(professor.getClass(), professor.getProfessorId());
                logSala.setProfessor(professor);
            }
            Sala sala = logSala.getSala();
            if (sala != null) {
                sala = em.getReference(sala.getClass(), sala.getSalaId());
                logSala.setSala(sala);
            }
            em.persist(logSala);
            if (assistente != null) {
                assistente.getLogSalaList().add(logSala);
                assistente = em.merge(assistente);
            }
            if (professor != null) {
                professor.getLogSalaList().add(logSala);
                professor = em.merge(professor);
            }
            if (sala != null) {
                sala.getLogSalaList().add(logSala);
                sala = em.merge(sala);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LogSala logSala) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LogSala persistentLogSala = em.find(LogSala.class, logSala.getLogSalaId());
            Assistente assistenteOld = persistentLogSala.getAssistente();
            Assistente assistenteNew = logSala.getAssistente();
            Professor professorOld = persistentLogSala.getProfessor();
            Professor professorNew = logSala.getProfessor();
            Sala salaOld = persistentLogSala.getSala();
            Sala salaNew = logSala.getSala();
            if (assistenteNew != null) {
                assistenteNew = em.getReference(assistenteNew.getClass(), assistenteNew.getAssistenteId());
                logSala.setAssistente(assistenteNew);
            }
            if (professorNew != null) {
                professorNew = em.getReference(professorNew.getClass(), professorNew.getProfessorId());
                logSala.setProfessor(professorNew);
            }
            if (salaNew != null) {
                salaNew = em.getReference(salaNew.getClass(), salaNew.getSalaId());
                logSala.setSala(salaNew);
            }
            logSala = em.merge(logSala);
            if (assistenteOld != null && !assistenteOld.equals(assistenteNew)) {
                assistenteOld.getLogSalaList().remove(logSala);
                assistenteOld = em.merge(assistenteOld);
            }
            if (assistenteNew != null && !assistenteNew.equals(assistenteOld)) {
                assistenteNew.getLogSalaList().add(logSala);
                assistenteNew = em.merge(assistenteNew);
            }
            if (professorOld != null && !professorOld.equals(professorNew)) {
                professorOld.getLogSalaList().remove(logSala);
                professorOld = em.merge(professorOld);
            }
            if (professorNew != null && !professorNew.equals(professorOld)) {
                professorNew.getLogSalaList().add(logSala);
                professorNew = em.merge(professorNew);
            }
            if (salaOld != null && !salaOld.equals(salaNew)) {
                salaOld.getLogSalaList().remove(logSala);
                salaOld = em.merge(salaOld);
            }
            if (salaNew != null && !salaNew.equals(salaOld)) {
                salaNew.getLogSalaList().add(logSala);
                salaNew = em.merge(salaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = logSala.getLogSalaId();
                if (findLogSala(id) == null) {
                    throw new NonexistentEntityException("The logSala with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LogSala logSala;
            try {
                logSala = em.getReference(LogSala.class, id);
                logSala.getLogSalaId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The logSala with id " + id + " no longer exists.", enfe);
            }
            Assistente assistente = logSala.getAssistente();
            if (assistente != null) {
                assistente.getLogSalaList().remove(logSala);
                assistente = em.merge(assistente);
            }
            Professor professor = logSala.getProfessor();
            if (professor != null) {
                professor.getLogSalaList().remove(logSala);
                professor = em.merge(professor);
            }
            Sala sala = logSala.getSala();
            if (sala != null) {
                sala.getLogSalaList().remove(logSala);
                sala = em.merge(sala);
            }
            em.remove(logSala);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LogSala> findLogSalaEntities() {
        return findLogSalaEntities(true, -1, -1);
    }

    public List<LogSala> findLogSalaEntities(int maxResults, int firstResult) {
        return findLogSalaEntities(false, maxResults, firstResult);
    }

    private List<LogSala> findLogSalaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LogSala.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LogSala findLogSala(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LogSala.class, id);
        } finally {
            em.close();
        }
    }

    public int getLogSalaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LogSala> rt = cq.from(LogSala.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
