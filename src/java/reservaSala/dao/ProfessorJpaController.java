/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reservaSala.dao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import reservaSala.model.bean.Gerente;
import reservaSala.model.bean.LogEquipamento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import reservaSala.dao.exceptions.IllegalOrphanException;
import reservaSala.dao.exceptions.NonexistentEntityException;
import reservaSala.model.bean.LogSala;
import reservaSala.model.bean.Agenda;
import reservaSala.model.bean.Professor;

/**
 *
 * @author Jorge
 */
public class ProfessorJpaController implements Serializable {

    public ProfessorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Professor professor) {
        if (professor.getLogEquipamentoList() == null) {
            professor.setLogEquipamentoList(new ArrayList<LogEquipamento>());
        }
        if (professor.getLogSalaList() == null) {
            professor.setLogSalaList(new ArrayList<LogSala>());
        }
        if (professor.getAgendaList() == null) {
            professor.setAgendaList(new ArrayList<Agenda>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gerente gerenteCadastro = professor.getGerenteCadastro();
            if (gerenteCadastro != null) {
                gerenteCadastro = em.getReference(gerenteCadastro.getClass(), gerenteCadastro.getGerenteId());
                professor.setGerenteCadastro(gerenteCadastro);
            }
            List<LogEquipamento> attachedLogEquipamentoList = new ArrayList<LogEquipamento>();
            for (LogEquipamento logEquipamentoListLogEquipamentoToAttach : professor.getLogEquipamentoList()) {
                logEquipamentoListLogEquipamentoToAttach = em.getReference(logEquipamentoListLogEquipamentoToAttach.getClass(), logEquipamentoListLogEquipamentoToAttach.getLogEquipamentoId());
                attachedLogEquipamentoList.add(logEquipamentoListLogEquipamentoToAttach);
            }
            professor.setLogEquipamentoList(attachedLogEquipamentoList);
            List<LogSala> attachedLogSalaList = new ArrayList<LogSala>();
            for (LogSala logSalaListLogSalaToAttach : professor.getLogSalaList()) {
                logSalaListLogSalaToAttach = em.getReference(logSalaListLogSalaToAttach.getClass(), logSalaListLogSalaToAttach.getLogSalaId());
                attachedLogSalaList.add(logSalaListLogSalaToAttach);
            }
            professor.setLogSalaList(attachedLogSalaList);
            List<Agenda> attachedAgendaList = new ArrayList<Agenda>();
            for (Agenda agendaListAgendaToAttach : professor.getAgendaList()) {
                agendaListAgendaToAttach = em.getReference(agendaListAgendaToAttach.getClass(), agendaListAgendaToAttach.getAgendaId());
                attachedAgendaList.add(agendaListAgendaToAttach);
            }
            professor.setAgendaList(attachedAgendaList);
            em.persist(professor);
            if (gerenteCadastro != null) {
                gerenteCadastro.getProfessorList().add(professor);
                gerenteCadastro = em.merge(gerenteCadastro);
            }
            for (LogEquipamento logEquipamentoListLogEquipamento : professor.getLogEquipamentoList()) {
                Professor oldProfessorOfLogEquipamentoListLogEquipamento = logEquipamentoListLogEquipamento.getProfessor();
                logEquipamentoListLogEquipamento.setProfessor(professor);
                logEquipamentoListLogEquipamento = em.merge(logEquipamentoListLogEquipamento);
                if (oldProfessorOfLogEquipamentoListLogEquipamento != null) {
                    oldProfessorOfLogEquipamentoListLogEquipamento.getLogEquipamentoList().remove(logEquipamentoListLogEquipamento);
                    oldProfessorOfLogEquipamentoListLogEquipamento = em.merge(oldProfessorOfLogEquipamentoListLogEquipamento);
                }
            }
            for (LogSala logSalaListLogSala : professor.getLogSalaList()) {
                Professor oldProfessorOfLogSalaListLogSala = logSalaListLogSala.getProfessor();
                logSalaListLogSala.setProfessor(professor);
                logSalaListLogSala = em.merge(logSalaListLogSala);
                if (oldProfessorOfLogSalaListLogSala != null) {
                    oldProfessorOfLogSalaListLogSala.getLogSalaList().remove(logSalaListLogSala);
                    oldProfessorOfLogSalaListLogSala = em.merge(oldProfessorOfLogSalaListLogSala);
                }
            }
            for (Agenda agendaListAgenda : professor.getAgendaList()) {
                Professor oldProfessorOfAgendaListAgenda = agendaListAgenda.getProfessor();
                agendaListAgenda.setProfessor(professor);
                agendaListAgenda = em.merge(agendaListAgenda);
                if (oldProfessorOfAgendaListAgenda != null) {
                    oldProfessorOfAgendaListAgenda.getAgendaList().remove(agendaListAgenda);
                    oldProfessorOfAgendaListAgenda = em.merge(oldProfessorOfAgendaListAgenda);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Professor professor) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Professor persistentProfessor = em.find(Professor.class, professor.getProfessorId());
            Gerente gerenteCadastroOld = persistentProfessor.getGerenteCadastro();
            Gerente gerenteCadastroNew = professor.getGerenteCadastro();
            List<LogEquipamento> logEquipamentoListOld = persistentProfessor.getLogEquipamentoList();
            List<LogEquipamento> logEquipamentoListNew = professor.getLogEquipamentoList();
            List<LogSala> logSalaListOld = persistentProfessor.getLogSalaList();
            List<LogSala> logSalaListNew = professor.getLogSalaList();
            List<Agenda> agendaListOld = persistentProfessor.getAgendaList();
            List<Agenda> agendaListNew = professor.getAgendaList();
            List<String> illegalOrphanMessages = null;
            for (LogEquipamento logEquipamentoListOldLogEquipamento : logEquipamentoListOld) {
                if (!logEquipamentoListNew.contains(logEquipamentoListOldLogEquipamento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LogEquipamento " + logEquipamentoListOldLogEquipamento + " since its professor field is not nullable.");
                }
            }
            for (LogSala logSalaListOldLogSala : logSalaListOld) {
                if (!logSalaListNew.contains(logSalaListOldLogSala)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LogSala " + logSalaListOldLogSala + " since its professor field is not nullable.");
                }
            }
            for (Agenda agendaListOldAgenda : agendaListOld) {
                if (!agendaListNew.contains(agendaListOldAgenda)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Agenda " + agendaListOldAgenda + " since its professor field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (gerenteCadastroNew != null) {
                gerenteCadastroNew = em.getReference(gerenteCadastroNew.getClass(), gerenteCadastroNew.getGerenteId());
                professor.setGerenteCadastro(gerenteCadastroNew);
            }
            List<LogEquipamento> attachedLogEquipamentoListNew = new ArrayList<LogEquipamento>();
            for (LogEquipamento logEquipamentoListNewLogEquipamentoToAttach : logEquipamentoListNew) {
                logEquipamentoListNewLogEquipamentoToAttach = em.getReference(logEquipamentoListNewLogEquipamentoToAttach.getClass(), logEquipamentoListNewLogEquipamentoToAttach.getLogEquipamentoId());
                attachedLogEquipamentoListNew.add(logEquipamentoListNewLogEquipamentoToAttach);
            }
            logEquipamentoListNew = attachedLogEquipamentoListNew;
            professor.setLogEquipamentoList(logEquipamentoListNew);
            List<LogSala> attachedLogSalaListNew = new ArrayList<LogSala>();
            for (LogSala logSalaListNewLogSalaToAttach : logSalaListNew) {
                logSalaListNewLogSalaToAttach = em.getReference(logSalaListNewLogSalaToAttach.getClass(), logSalaListNewLogSalaToAttach.getLogSalaId());
                attachedLogSalaListNew.add(logSalaListNewLogSalaToAttach);
            }
            logSalaListNew = attachedLogSalaListNew;
            professor.setLogSalaList(logSalaListNew);
            List<Agenda> attachedAgendaListNew = new ArrayList<Agenda>();
            for (Agenda agendaListNewAgendaToAttach : agendaListNew) {
                agendaListNewAgendaToAttach = em.getReference(agendaListNewAgendaToAttach.getClass(), agendaListNewAgendaToAttach.getAgendaId());
                attachedAgendaListNew.add(agendaListNewAgendaToAttach);
            }
            agendaListNew = attachedAgendaListNew;
            professor.setAgendaList(agendaListNew);
            professor = em.merge(professor);
            if (gerenteCadastroOld != null && !gerenteCadastroOld.equals(gerenteCadastroNew)) {
                gerenteCadastroOld.getProfessorList().remove(professor);
                gerenteCadastroOld = em.merge(gerenteCadastroOld);
            }
            if (gerenteCadastroNew != null && !gerenteCadastroNew.equals(gerenteCadastroOld)) {
                gerenteCadastroNew.getProfessorList().add(professor);
                gerenteCadastroNew = em.merge(gerenteCadastroNew);
            }
            for (LogEquipamento logEquipamentoListNewLogEquipamento : logEquipamentoListNew) {
                if (!logEquipamentoListOld.contains(logEquipamentoListNewLogEquipamento)) {
                    Professor oldProfessorOfLogEquipamentoListNewLogEquipamento = logEquipamentoListNewLogEquipamento.getProfessor();
                    logEquipamentoListNewLogEquipamento.setProfessor(professor);
                    logEquipamentoListNewLogEquipamento = em.merge(logEquipamentoListNewLogEquipamento);
                    if (oldProfessorOfLogEquipamentoListNewLogEquipamento != null && !oldProfessorOfLogEquipamentoListNewLogEquipamento.equals(professor)) {
                        oldProfessorOfLogEquipamentoListNewLogEquipamento.getLogEquipamentoList().remove(logEquipamentoListNewLogEquipamento);
                        oldProfessorOfLogEquipamentoListNewLogEquipamento = em.merge(oldProfessorOfLogEquipamentoListNewLogEquipamento);
                    }
                }
            }
            for (LogSala logSalaListNewLogSala : logSalaListNew) {
                if (!logSalaListOld.contains(logSalaListNewLogSala)) {
                    Professor oldProfessorOfLogSalaListNewLogSala = logSalaListNewLogSala.getProfessor();
                    logSalaListNewLogSala.setProfessor(professor);
                    logSalaListNewLogSala = em.merge(logSalaListNewLogSala);
                    if (oldProfessorOfLogSalaListNewLogSala != null && !oldProfessorOfLogSalaListNewLogSala.equals(professor)) {
                        oldProfessorOfLogSalaListNewLogSala.getLogSalaList().remove(logSalaListNewLogSala);
                        oldProfessorOfLogSalaListNewLogSala = em.merge(oldProfessorOfLogSalaListNewLogSala);
                    }
                }
            }
            for (Agenda agendaListNewAgenda : agendaListNew) {
                if (!agendaListOld.contains(agendaListNewAgenda)) {
                    Professor oldProfessorOfAgendaListNewAgenda = agendaListNewAgenda.getProfessor();
                    agendaListNewAgenda.setProfessor(professor);
                    agendaListNewAgenda = em.merge(agendaListNewAgenda);
                    if (oldProfessorOfAgendaListNewAgenda != null && !oldProfessorOfAgendaListNewAgenda.equals(professor)) {
                        oldProfessorOfAgendaListNewAgenda.getAgendaList().remove(agendaListNewAgenda);
                        oldProfessorOfAgendaListNewAgenda = em.merge(oldProfessorOfAgendaListNewAgenda);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = professor.getProfessorId();
                if (findProfessor(id) == null) {
                    throw new NonexistentEntityException("The professor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Professor professor;
            try {
                professor = em.getReference(Professor.class, id);
                professor.getProfessorId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The professor with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<LogEquipamento> logEquipamentoListOrphanCheck = professor.getLogEquipamentoList();
            for (LogEquipamento logEquipamentoListOrphanCheckLogEquipamento : logEquipamentoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Professor (" + professor + ") cannot be destroyed since the LogEquipamento " + logEquipamentoListOrphanCheckLogEquipamento + " in its logEquipamentoList field has a non-nullable professor field.");
            }
            List<LogSala> logSalaListOrphanCheck = professor.getLogSalaList();
            for (LogSala logSalaListOrphanCheckLogSala : logSalaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Professor (" + professor + ") cannot be destroyed since the LogSala " + logSalaListOrphanCheckLogSala + " in its logSalaList field has a non-nullable professor field.");
            }
            List<Agenda> agendaListOrphanCheck = professor.getAgendaList();
            for (Agenda agendaListOrphanCheckAgenda : agendaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Professor (" + professor + ") cannot be destroyed since the Agenda " + agendaListOrphanCheckAgenda + " in its agendaList field has a non-nullable professor field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Gerente gerenteCadastro = professor.getGerenteCadastro();
            if (gerenteCadastro != null) {
                gerenteCadastro.getProfessorList().remove(professor);
                gerenteCadastro = em.merge(gerenteCadastro);
            }
            em.remove(professor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Professor> findProfessorEntities() {
        return findProfessorEntities(true, -1, -1);
    }

    public List<Professor> findProfessorEntities(int maxResults, int firstResult) {
        return findProfessorEntities(false, maxResults, firstResult);
    }

    private List<Professor> findProfessorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Professor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Professor findProfessor(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Professor.class, id);
        } finally {
            em.close();
        }
    }

    public int getProfessorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Professor> rt = cq.from(Professor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
        public Professor findProfessorByLogin(String nome) {
        Professor professor = null;
        EntityManager em = getEntityManager();
        Query query = em.createNamedQuery("Professor.findByNome");
        query.setParameter("nome", nome);
        try {
            professor = (Professor) query.getSingleResult();
        } 
        catch (NoResultException nre) {
        }
        return professor;
        }
}
