<%-- 
    Document   : sucessoCadastro
    Created on : 29/03/2016, 02:14:46
    Author     : Jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
    <!--<![endif]-->

    <%@include file='head.html'%>

    <body>
        <div class="wrapper box row row-offcanvas row-offcanvas-left">
            <%@include file='sidebar.jsp'%>
            <%@include file='upperbar-right.jsp'%> 


        </div><!-- /top nav -->
        <h1>Cadastro Realizado com sucesso!</h1>

        <div class="col-sm-offset-2 col-sm-10">
            <form action="home.jsp">
                <button type="submit" class="btn btn-primary">Voltar</button>
            </form>    
        </div>
    </body>
</html>
