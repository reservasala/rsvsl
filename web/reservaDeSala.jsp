<%-- 
    Document   : reservaDeSala
    Created on : Mar 29, 2016, 9:09:23 PM
    Author     : alexandre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
    <!--<![endif]-->

    <%@include file='head.html'%>

    <body>
        <div class="wrapper box row row-offcanvas row-offcanvas-left">
            <%@include file='sidebar.jsp'%>
            
                <ul class="nav navbar-nav">
                    <li class="aba active" id="aba-info-basica">
                        <a href="#" role="tab" data-toggle="tab">Criar</a>
                    </li>
                </ul>

                <%@include file='upperbar-right.jsp'%> 
            
                    </nav>
                </div><!-- /top nav -->

                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->

                        <div class="row panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">
                                <h3 class="panel-title">Reservar Sala</h3>
                            </div>
                            <form id="meu_form" name="reservaSala" action="ServletController" class="form-horizontal" role="form" method="POST">
                                
                                <input type="hidden" name="cmd" value="reservaSala.model.command.ReservaDeSala" />
                                
                                <div class="form-group">
                                    <label for="input-sala-ID" class="col-sm-2 control-label">Nº da Sala</label>
                                    <div class="col-sm-6">
                                        <input name="idSala" type="text" class="form-control" id="idSala" placeholder="Ex.: 1234">
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                    <label for="input-horario-ini" class="col-sm-2 control-label">Data de Inicio</label>
                                    <div class="col-sm-6">
                                        <input name="dataHoraChaveRet" type="text" class="form-control" id="dataHoraChaveRet" placeholder="Ex.: DD/MM/YYYY HH:mm">
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-horario-fim" class="col-sm-2 control-label">Data de Fim</label>
                                    <div class="col-sm-6">
                                        <input name="dataHoraChaveDevo" type="text" class="form-control" id="dataHoraChaveDevo" placeholder="Ex.: DD/MM/YYYY HH:mm">
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary">Reservar</button>
                                    </div>
                                </div>
                            </form>


                        </div><!--/row-->
                        <hr>

                    </div><!-- /col-9 -->
                </div><!-- /padding -->
                <c:if test="${msg}">
                <!-- Modal -->
                  <div class="modal fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Aten&ccedil;&atilde;o</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                ${msg_txt}
                            </div>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                      </div>
                    </div>
                  </div>  
              </c:if>
            </div><!-- /main -->
        </div><!--post modal-->

        <script>
            window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')
        </script> <script src="js/vendor/bootstrap.min.js"></script> <script src="js/vendor/moment.min.js"></script> <script src="js/main.js"></script> <script src="js/tools.js"></script>
    </body>
</html>