<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->

<html class="no-js">
    <!--<![endif]-->

    <%@include file='head.html'%>

    <body>
        <div class="wrapper box row row-offcanvas row-offcanvas-left">
            <%@include file='sidebar.jsp'%>

            <ul class="nav navbar-nav">
                <li id="aba-solicitado" class="aba active">
                    <a href="#pane-solicitado" role="tab" data-toggle="tab">Agendamentos Solicitados</a>
                </li>

                <li id="aba-atendido" class="aba">
                    <a href="#pane-atendido" role="tab" data-toggle="tab">Agendamentos Atendidos </a>
                </li>
                
                 <li id="aba-finalizado" class="aba">
                    <a href="#pane-finalizado" role="tab" data-toggle="tab">Agendamentos Finalizados
                    </a>
                </li>
                
                <li id="aba-recusado" class="aba">
                    <a href="#pane-recusado" role="tab" data-toggle="tab">Agendamentos Recusados
                    </a>
                </li>
            </ul>
        </nav>
    </div><!-- /top nav -->
    
    <c:if test="${pageContext.session.getAttribute('isGerente').equals('true')}">
            <div class="padding">
            <div class="full col-sm-9">
                <!-- content -->

                <div class="row tab-content">

                    <!-- tabela de todos os solicitados -->
                    <div class="tab-pane active"  id="pane-solicitado">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Solicitadas
                            </div>
                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th class="col-sm-2">Sala</th>
                                    <th class="col-sm-6">Professor</th>
                                    <th class="col-sm-2">Data</th>
                                </tr>
                                <c:forEach var="agenda" items="${listaSolicitados}">
                                    <tr class="linha-agenda" id="${agenda.getAgendaId()}">
                                        <td class="linha-agenda-numero">${agenda.getSala().numero}</td>
                                         <td class="linha-agenda-nome">${agenda.getProfessor().nome}</td>
                                        <td class="linha-agenda-data">${agenda.dataHoraInicio }</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>

                    <!-- tabela de todos os atendidos -->
                    <div class="tab-pane"  id="pane-atendido">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Atendidas
                            </div>

                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th class="col-sm-2">Sala</th>
                                    <th class="col-sm-6">Professor</th>
                                    <th class="col-sm-2">Data</th>                                   
                                </tr>

                                <c:forEach var="agenda" items="${listaAtendidas}">
                                    <tr class="linha-agenda" id="${agenda.getAgendaId()}">
                                        <td class="linha-agenda-numero">${agenda.getSala().numero}</td>                                       
                                        <td class="linha-agenda-nome">${agenda.getProfessor().nome}</td>
                                        <td class="linha-agenda-data">${agenda.dataHoraInicio }</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                    
                     <div class="tab-pane"  id="pane-finalizado">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Finalizado
                            </div>

                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th class="col-sm-2">Sala</th>
                                    <th class="col-sm-6">Professor</th>
                                    <th class="col-sm-2">Data</th>                                   
                                </tr>

                                <c:forEach var="agenda" items="${listaFinalizados}">
                                    <tr class="linha-agenda" id="${agenda.getAgendaId()}">
                                        <td class="linha-agenda-numero">${agenda.getSala().numero}</td>
                                        <td class="linha-agenda-nome">${agenda.getProfessor().nome}</td>
                                        <td class="linha-agenda-data">${agenda.dataHoraInicio }</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                    
                    <div class="tab-pane"  id="pane-recusado">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Recusados
                            </div>

                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th class="col-sm-2">Sala</th>
                                    <th class="col-sm-6">Professor</th>
                                    <th class="col-sm-2">Data</th>                                   
                                </tr>

                                <c:forEach var="agenda" items="${listaRecusados}">
                                    <tr class="linha-agenda" id="${agenda.getAgendaId()}">
                                        <td class="linha-agenda-numero">${agenda.getSala().numero}</td>
                                        <td class="linha-agenda-nome"> ${agenda.getProfessor().nome}</td>
                                        <td class="linha-agenda-data">${agenda.dataHoraInicio }</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>

                
                    

                </div><!--/row-->
                <hr>

            </div><!-- /col-9 -->
        </div><!-- /padding -->
    </c:if>
    

    <c:if test="${pageContext.session.getAttribute('isProfessor').equals('true')}">
                <div class="padding">
            <div class="full col-sm-9">
                <!-- content -->

                <div class="row tab-content">

                    <!-- tabela de todos os analise -->
                    <div class="tab-pane active"  id="pane-solicitado">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Solicitadas
                            </div>

                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th class="col-sm-2">Sala</th>                                  
                                    <th class="col-sm-2">Data</th>
                                </tr>

                                <c:forEach var="agenda" items="${listaSolicitados}">
                                    <tr class="linha-agenda" id="${agenda.getAgendaId()}">
                                        <td class="linha-agenda-numero">${agenda.getSala().numero}</td>
                                        <td class="linha-agenda-data">${agenda.dataHoraInicio }</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>

                    <!-- tabela de todos os confirmados -->
                    <div class="tab-pane"  id="pane-atendido">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Atendidas
                            </div>

                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th class="col-sm-2">Sala</th>
                                    <th class="col-sm-2">Data</th>                                   
                                </tr>

                                <c:forEach var="agenda" items="${listaAtendidas}">
                                    <tr class="linha-agenda" id="${agenda.getAgendaId()}">
                                        <td class="linha-agenda-numero">${agenda.getSala().numero}</td>                                       
                                        <td class="linha-agenda-data">${agenda.dataHoraInicio }</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                    
                    <div class="tab-pane"  id="pane-recusado">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Recusados
                            </div>

                            <table class="table table-bordered table-striped table-hover">
                                <tr>
                                    <th class="col-sm-2">Sala</th>
                                    <th class="col-sm-2">Data</th>                                   
                                </tr>

                                <c:forEach var="agenda" items="${listaRecusados}">
                                    <tr class="linha-agenda" id="${agenda.getAgendaId()}">
                                        <td class="linha-agenda-numero">${agenda.getSala().numero}</td>
                                        <td class="linha-agenda-data">${agenda.dataHoraInicio }</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>

                
                    

                </div><!--/row-->
                <hr>

            </div><!-- /col-9 -->
        </div><!-- /padding -->
    </c:if>
</div><!-- /main -->
</div><!--post modal-->

<script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')
</script> 
<script src="js/vendor/bootstrap.min.js"></script> 
<script src="js/vendor/moment.min.js"></script> 
<script src="js/main.js"></script> 
<script src="js/salaList.js"></script>
</body>
</html>