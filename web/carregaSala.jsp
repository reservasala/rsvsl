<%-- 
    Document   : cadastraProfessor
    Created on : 29/03/2016, 01:45:12
    Author     : Jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
    <!--<![endif]-->

    <%@include file='head.html'%>

    <body>
        <div class="wrapper box row row-offcanvas row-offcanvas-left">
            <%@include file='sidebar.jsp'%>
            
                <ul class="nav navbar-nav">
                    <li class="aba active" id="aba-info-basica">
                        <a href="#" role="tab" data-toggle="tab"></a>
                    </li>
                </ul>

                <%@include file='upperbar-right.jsp'%> 
            
                    
                </div><!-- /top nav -->

                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->
                        <div class="row panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">
                                <h3 class="panel-title">Sala</h3>
                            </div>
                    <table class="table table-bordered table-striped table-hover">

                        <tr class="">
                            <td class="">N&uacute;mero</td>
                            <td class="">
                                ${agenda.getSala().numero}
                            </td>                                   
                        </tr>
                        
                        <tr class="">
                            <td class="">Pr&eacute;dio</td>
                            <td class="">
                                ${agenda.getSala().predio}
                            </td>                                   
                        </tr>                        
                        
                        <tr class="">
                            <td class="">Professor</td>
                            <td class="">
                                ${agenda.getProfessor().nome}
                            </td>                                   
                        </tr>

                        <tr class="">
                            <td class="">Data In&iacute;cio</td>
                            <td class="">
                                ${agenda.dataHoraInicio}
                            </td>                                   
                        </tr>                        
                        
                        
                        <tr class="">
                            <td class="">Data Fim</td>
                            <td class="">
                                ${agenda.dataHoraFim}
                            </td>                                   
                        </tr>            
                        
                    </table>
                </div><!--/row-->
                
           
                <c:if test="${(pageContext.session.getAttribute('isGerente').equals('true') && status.equals ('C'))}">
                <form name="edit" action="ServletController" method="POST">
                    <input type="hidden" name="cmd" value="reservaSala.model.command.EditAgenda" />
                    <input type="hidden" name="acao" value="autorizar" />
                    <input type="hidden" name="id" value=${agendaId} />
                    <input type="submit" value="Autorizar" class="btn btn-primary btn pull-right"/>
                </form>
                
                <form name="edit" action="ServletController" method="POST">
                    <input type="hidden" name="cmd" value="reservaSala.model.command.EditAgenda" />
                    <input type="hidden" name="acao" value="recusar" />
                    <input type="hidden" name="id" value=${agendaId} />
                    <input type="submit" value="Recusar" class="btn btn-danger btn pull-right"/>
                </form>
                </c:if>
              
                
                
                <c:if test="${pageContext.session.getAttribute('isProfessor').equals('true')}">
                <form name="edit" action="ServletController" method="POST">
                    <input type="hidden" name="cmd" value="reservaSala.model.command.LoadSalaReservada" />
                    <input type="submit" value="Retornar" class="btn btn-primary btn pull-right"/>
                </form>
                </c:if>
                <hr>
            </div>

                        </div><!--/row-->
                        <hr>

                    </div><!-- /col-9 -->
                </div>
                            
                
                            
                <!-- /padding -->
                <c:if test="${msg}">
                <!-- Modal -->
                  <div class="modal fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Aten&ccedil;&atilde;o</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                ${msg_txt}
                            </div>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                      </div>
                    </div>
                  </div>  
              </c:if>
            </div><!-- /main -->
        </div><!--post modal-->

        <script>
            window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')
        </script> <script src="js/vendor/bootstrap.min.js"></script> 
        <script src="js/vendor/moment.min.js"></script> 
        <script src="js/main.js"></script> 
        <script src="js/tools.js"></script>
    </body>
</html>