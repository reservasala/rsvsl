<%-- 
    Document   : home
    Created on : 07/03/2016, 18:39:10
    Author     : Jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>

    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Realizado </title>
        <%@include file='head.html'%>
    
    <body>
        <div class="wrapper box row row-offcanvas row-offcanvas-left">
            <%@include file='sidebar.jsp'%>

            <ul class="nav navbar-nav">
                <li class="aba active" id="aba-info-basica">
                    <a href="#" role="tab" data-toggle="tab">Dashboard</a>
                </li>
            </ul>

            <%@include file='upperbar-right.jsp'%> 

        
    </div><!-- /top nav -->

    <div class="padding">
        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-top: 12px;">
            <h3 style="text-align:center;"></h3>
            <div id="projetosTotais" style="width:450px;height:450px">
                
            </div>
            <div id="projetosTotais-valores" style="display: none">
                <div id="criados">${criados}</div>
                <div id="emConfig">${emConfig}</div>
                <div id="configurado">${configurado}</div>
                <div id="emTeste">${emTeste}</div>
                <div id="emAnalise">${emAnalise}</div>
                <div id="analisado">${analisado}</div>
                <div id="validadoAuditor">${validadoAuditor}</div>
                <div id="finalizado">${finalizado}</div>
            </div>
        </div>
        
        <c:if test="${isGerente == 'true'}">
            <div class="col-md-6 col-sm-6 col-xs-6" style="padding-top: 12px;">
                <h3 style="text-align:center;"></h3>
                <div id="projetosPorUsuarios" style="width:450px;height:450px">

                </div>
                <div id="projetosPorUsuarios-valores" style="display: none">
                    <c:forEach var="usuario" items="${usuarios}">
                        <p class="usuario" nome="${usuario.nome}" projetos="${usuario.getProjetosList().size()}"></p>
                    </c:forEach>
                </div>
            </div>
        </c:if>
            
        <div class="col-md-6 col-sm-6 col-xs-6" style="padding-top: 12px;">
            <h3 style="text-align:center;"></h3>
            <div id="projetosFinalizados" style="width:450px;height:450px">
                
            </div>
            <div id="projetosFinalizados-valores" style="display: none">
                <div id="naoFinalizado">${naoFinalizado}</div>
                <div id="naoRodando">${naoRodando}</div>
            </div>
        </div>
        <!-- /padding -->
    </div><!-- /main -->
</div><!--post modal-->

<script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')
</script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/moment.min.js"></script> 
<script src="js/vendor/flot/jquery.flot.js"></script> 
<script type="text/javascript" src="js/vendor/flot/jquery.flot.pie.js"></script>
<script src="js/main.js"></script>
<script src="js/home.js"></script>
</body>

