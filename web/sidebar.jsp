<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="column col-sm-2 col-xs-1 sidebar-offcanvas hidden-print" id="sidebar">
    <ul class="nav">
        <li>
            <a class=
               "visible-xs text-center glyphicon glyphicon-chevron-right"
               data-toggle="offcanvas" href="#" style=
               "font-style: italic" id="sidebar-seta"></a>
        </li>
    </ul>

    <ul class="nav hidden-xs" id="lg-menu">
        <li>
            <a class="side-menu-title" href="home.jsp"><i class="glyphicon glyphicon-home"></i> Home</a>
        </li>
        <li>
            <a class="side-menu-title" data-toggle="collapse" data-parent="#accordion" href="#projetos"><i class="glyphicon glyphicon-stats"></i> Cadastrar</a>
        </li>
        <div id="projetos" class="panel-collapse collapse out">
            <div class="panel-body side-menu-panel">
                <table class="table table-hover">
                    <c:if test="${pageContext.session.getAttribute('isGerente') == 'true'}">
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-pencil"></span><a href="cadastraProfessor.jsp">Cadastrar Professor </a>
                            </td>
                        </tr>
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-pencil"></span><a href="cadastraSala.jsp">Cadastrar Sala</a>
                            </td>
                        </tr>
                        <tr>   
                            <td><%--
                                <span class="glyphicon glyphicon-list"></span><a href="ServletController?cmd=reservaSala.model.command.LoadProjectListAsGerente">Listar como Gerente</a>
                            </td>--%>
                        </tr>
                    </c:if>
                    <c:if test="${isOperador == 'true'}">
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-list"></span><a href="ServletController?cmd=reservaSala.model.command.LoadProjectListAsOperador">Listar como Operador</a>
                            </td>
                        </tr>
                    </c:if>
                    <c:if test="${isAnalista == 'true'}">
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-list"></span><a href="ServletController?cmd=reservaSala.model.command.LoadProjectListAsAnalista">Listar como Analista</a>
                            </td>
                        </tr>
                    </c:if>
                    <c:if test="${isAuditor == 'true'}">
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-list"></span><a href="ServletController?cmd=reservaSala.model.command.LoadProjectListAsAuditor">Listar como Auditor</a>
                            </td>
                        </tr>
                    </c:if>    
                    <c:if test="${pageContext.session.getAttribute('isAdmin') == 'true'}">
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-lock"></span><a href="ServletController?cmd=reservaSala.model.command.LoadProjectListAsAdmin">Listar como Admin</a>
                            </td>
                        </tr>    
                    </c:if>
                </table>
            </div>
        </div>

        <c:if test="${isProfessor == 'true' || isGerente == 'true' }">
            <li>
                <a class="side-menu-title" data-toggle="collapse" data-parent="#accordion" href="#vms"><i class="glyphicon glyphicon-cloud"></i>
                    Reservas</a>
            </li>
            <div id="vms" class="panel-collapse collapse out">
                <div class="panel-body side-menu-panel">
                    <table class="table table-hover">
                        <c:if test="${isProfessor == 'true'}">
                        <tr>
                            <td>
                                <span class="glyphicon glyphicon-list"></span><a href="reservaDeSala.jsp">Reservar Sala</a>
                            </td>
                        </tr>
                        </c:if>
                        
                        <tr>
                            <td>
                                <span class="glyphicon glyphicon-pencil"></span><a href="ServletController?cmd=reservaSala.model.command.LoadSalaReservada">Salas Reservadas</a>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
        </c:if>
        
        
        <c:if test="${isGerente == 'true'}">
           <%-- <li>
                <a class="side-menu-title" data-toggle="collapse" data-parent="#accordion" href="#clientes"><i class=
                                                                                                               "glyphicon glyphicon-briefcase"></i> Clientes</a>
            </li>--%>
            <div id="clientes" class="panel-collapse collapse out">
                <div class="panel-body side-menu-panel">
                    <table class="table table-hover">
                        <tr>
                            <td>
                                <span class="glyphicon glyphicon-list"></span><a href="ServletController?cmd=reservaSala.model.command.LoadClientList">Listar</a>
                            </td>
                        </tr>
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-pencil"></span><a href="ServletController?cmd=reservaSala.model.command.EditClient">Cadastrar</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>        
        </c:if>
     
        <c:if test="${isAdmin == 'true' || isGerente == 'true' }">    
        <%--    <li>
                <a class="side-menu-title" data-toggle="collapse" data-parent="#accordion" href="#usuarios"><i class="glyphicon glyphicon-user"></i>
                    Usu&aacute;rios</a>
            </li>--%>
            <div id="usuarios" class="panel-collapse collapse out">
                <div class="panel-body side-menu-panel">
                    <table class="table table-hover">
                        <c:if test="${isAdmin == 'true'}">
                            <tr>
                                <td>
                                    <span class="glyphicon glyphicon-pencil"></span><a href="ServletController?cmd=reservaSala.model.command.EditUser">Criar</a>
                                </td>
                            </tr>
                        </c:if>
                        <tr>
                            <td>
                                <span class="glyphicon glyphicon-list"></span><a href="ServletController?cmd=reservaSala.model.command.LoadUserList">Listar</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </c:if>


<%--  
        <li>
            <a class="side-menu-title" data-toggle="collapse" data-parent="#accordion" href="#ferramentas"><i class=
                                                                                                              "glyphicon glyphicon-wrench"></i> Ferramentas</a>
        </li>--%>
        <div id="ferramentas" class="panel-collapse collapse out">
            <div class="panel-body side-menu-panel">
                <table class="table table-hover">
                    <tr>
                        <td>
                            <span class="glyphicon glyphicon-list"></span><a href="ServletController?cmd=reservaSala.model.command.LoadToolList">Listar</a>
                        </td>
                    </tr>
                    <c:if test="${isAdmin == 'true'}">
                        <tr>   
                            <td>
                                <span class="glyphicon glyphicon-pencil"></span><a href="ServletController?cmd=reservaSala.model.command.EditTool">Criar</a>
                            </td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </div>


    </ul>

    <ul class="nav visible-xs" id="xs-menu">
        <li>
            <a class="text-center glyphicon glyphicon-stats" href=
               "#ferramentas" style="font-style: italic"></a>
        </li>

        <li>
            <a class="text-center glyphicon glyphicon-home" href=
               "#" style="font-style: italic"></a>
        </li>

        <li>
            <a class="text-center glyphicon glyphicon-user" href=
               "#" style="font-style: italic"></a>
        </li>

        <li>
            <a class="text-center glyphicon glyphicon-wrench" href="#"
               style="font-style: italic"></a>
        </li>
    </ul>
</div><!-- /sidebar -->


<!-- main right col -->

<div class="column col-sm-10 col-xs-11" id="main">
    <!-- top nav -->

    <div class="navbar navbar-blue navbar-static-top">
        <div class="navbar-header">
            <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Reserva de Sala</a>
        </div>

        <nav class="collapse navbar-collapse">