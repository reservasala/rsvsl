<%-- 
    Document   : cadastraProfessor
    Created on : 29/03/2016, 01:45:12
    Author     : Jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
    <!--<![endif]-->

    <%@include file='head.html'%>

    <body>
        <div class="wrapper box row row-offcanvas row-offcanvas-left">
            <%@include file='sidebar.jsp'%>
            
                <ul class="nav navbar-nav">
                    <li class="aba active" id="aba-info-basica">
                        <a href="#" role="tab" data-toggle="tab">Criar</a>
                    </li>
                </ul>

                <%@include file='upperbar-right.jsp'%> 
            
                    </nav>
                </div><!-- /top nav -->

                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->

                        <div class="row panel panel-primary">
                            <!-- Default panel contents -->
                            <div class="panel-heading">
                                <h3 class="panel-title">Novo Professor</h3>
                            </div>
                            <form id="meu_form" name="createUser" action="ServletController" class="form-horizontal" role="form" method="POST">
                                
                                <input type="hidden" name="cmd" value="reservaSala.model.command.CreateProfessor" />
                                
                                <div class="form-group">
                                    <label for="input-usuario-nome" class="col-sm-2 control-label">Nome</label>
                                    <div class="col-sm-6">
                                        <input name="name" type="name" class="form-control" id="inputUserName" placeholder="Ex.: Fulano da Silva Santos">
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                    <label for="input-usuario-cpf" class="col-sm-2 control-label">CPF</label>
                                    <div class="col-sm-6">
                                        <input name="cpf" type="text" class="form-control" id="inputUserCpf" placeholder="Ex.: 12345678910">
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="input-usuario-password" class="col-sm-2 control-label">Endereço</label>
                                    <div class="col-sm-6">
                                        <input name="endereco" type="text" class="form-control" id="inputUserEnd" placeholder="Ex.: R. Passo da Patria, 156">
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                                    </div>
                                </div>
                            </form>


                        </div><!--/row-->
                        <hr>

                    </div><!-- /col-9 -->
                </div><!-- /padding -->
                <c:if test="${msg}">
                <!-- Modal -->
                  <div class="modal fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title" id="myModalLabel">Aten&ccedil;&atilde;o</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                ${msg_txt}
                            </div>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                      </div>
                    </div>
                  </div>  
              </c:if>
            </div><!-- /main -->
        </div><!--post modal-->

        <script>
            window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')
        </script> <script src="js/vendor/bootstrap.min.js"></script> <script src="js/vendor/moment.min.js"></script> <script src="js/main.js"></script> <script src="js/tools.js"></script>
    </body>
</html>